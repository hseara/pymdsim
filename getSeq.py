#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Version 0.02
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Sequence file generator.
"""

import os
import sys
import pybel
import openbabel as ob

from optparse import OptionParser
from prepReader import mkProperty, readPrepFile



mkProperty(ob.OBAtom, 'shadow')



class Vertex(object):
    """Vertex object for representing a residue.

    """

    def __init__(self, parent, num, name):
        """Initialize a new vertex object.

        """
        self.parent      = parent
        self.num         = num
        self.name        = name
        self.glycam_name = None
        self.branches    = []
        self.tags        = []


    def __str__(self):
        """String representiation for the vertex.

        """
        return self.name + '-' + str(self.num)



class Edge(object):
    """Edge object for representing a connection between residues.

    """

    def __init__(self, start, end, conf):
        """Initialize a new vertex object.

        """
        self.start = start
        self.end   = end
        self.conf  = conf



class Graph(object):
    """Graph object for representing a tree of residues.

    """

    def __init__(self):
        """Initialize a new graph object.

        """
        self.vertices = []
        self.edges    = []


    def add_vertex(self, vertice):
        """Add vertex to the graph.

        """
        self.vertices.append(vertice)


    def add_edge(self, edge):
        """Add edge to the graph.

        """
        if edge.end not in edge.start.branches:
            edge.start.branches.append(edge.end)
        self.edges.append(edge)


    def get_edge(self, start, end):
        """Get the given edge from the graph.

        """
        for e in self.edges:
            if e.start == start and e.end == end:
                return e
        raise Exception("Edge ({},{}) not found!".format(start, end))



def optP():
    """Create and initialize Option Parser.

    """
    usage = '[python] %prog [-v] (-a|-r) a1 b1 [a2 b2 ...] -c input_file ' \
            '[-p residues1.prep [residues2.prep ...]] -o output.seq'
    version = '\n%prog Version 0.02\n\nRequires Python 2.6 ' \
              'or newer.'
    description = 'Generate sequence file from coordinate file. If PREP ' \
                  'file is provided, the residue names are taken from it ' \
                  'if possible.'
    optParser = OptionParser(usage=usage,
                             version=version,
                             description=description)

    optParser.set_defaults(inFN=None, outFN=None, prepFNs=None,
                           verbose=False, roots=[])

    optParser.add_option('-v', action='store_true',
                         dest='verbose',
                         help="Be verbose [default: %default]")

    optParser.add_option('-c', type='str',
                         dest='inFN',
                         help="Coordinate file (in) [default: %default]")
    optParser.add_option('-o', type='str',
                         dest='outFN',
                         help="Sequence file (out) [default: %default]")

    optParser.add_option('-a', action='callback',
                         callback=getIntParams,
                         dest='roots',
                         help="Sugar attachment bonds [default: %default]")
    optParser.add_option('-r', action='callback',
                         callback=getStrParams,
                         dest='roots',
                         help="Protein-Sugar residues [default: %default]")

    optParser.add_option('-p', action='callback',
                         callback=getStrParams,
                         dest='prepFNs',
                         help="Prep file(s) (in) [default: %default]")

    options, args = optParser.parse_args()

    if len(options.roots) < 2 or len(options.roots) % 2 != 0:
        print("Error: Flags -a and -r need even number of parameters!")
        sys.exit(1)

    if options.inFN is None:
        print("Error: Missing input file (-c)!")
        sys.exit(1)

    if  options.outFN is None:
        print("Error: Missing output file (-o)!")
        sys.exit(1)

    if not os.path.isfile(options.inFN):
        print("Error: Input file '{}' not found!".format(options.inFN))
        sys.exit(1)

    return options, args


def isInt(s):
    """Check if the given string represents an integer.

    """
    try:
        int(s)
        return True
    except ValueError:
        return False


def getIntParams(option, opt_str, value, parser):
    """Extract specified number of parameters from command line.

    Return a list of integers.
    """
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if ((arg[:2] == '--' and len(arg) >= 2) or
            (arg[:1] == '-' and len(arg) >= 1 and not isInt(arg)) or
            (not isInt(arg))):
            break
        else:
            value.append(int(arg))
            del rargs[0]
    setattr(parser.values, option.dest, value)


def getStrParams(option, opt_str, value, parser):
    """Extract specified number of parameters from command line.

    Return a list of strings.
    """
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if ((arg[:2] == '--' and len(arg) >= 2) or
            (arg[:1] == '-' and len(arg) >= 1)):
            break
        else:
            value.append(arg)
            del rargs[0]
    setattr(parser.values, option.dest, value)


def getNbrInRes(atom, maxdepth, depth=0, path=None, visited=None):
    """Get a string of length maxdepth of heavy atoms in the current residue.

    """
    resn = atom.GetResidue().GetNum()
    visited.append(atom.GetIndex())
    finished = False

    for a in ob.OBAtomAtomIter(atom):
        if (a.GetResidue().GetNum() == resn
            and not a.IsHydrogen()
            and a.GetIndex() not in visited):

            if depth < maxdepth:
                finished = getNbrInRes(a, maxdepth, depth+1, path, visited)
                if not finished:
                    continue
                else:
                    path.append(a)
                    break
            else:
                finished = True
                path.append(a)
                break

    return finished


def getConf(options, mol, start, end):
    """Get the conformation of the given connection.

    """
    # Check if start and end is connected
    success = False
    for a in ob.OBAtomAtomIter(start):
        if a == end:
            success = True
            break

    if not success:
        print("Error: Start and end atoms are not connected!")
        print("Start:", start.GetResidue().GetAtomID(start), start.GetIdx())
        print("End:  ", end.GetResidue().GetAtomID(end), end.GetIdx())
        sys.exit(2)

    # Get path in the starting residue
    spath = []
    success = getNbrInRes(start, 2, path=spath, visited=[])

    if options.verbose:
        print("Start path", len(spath))
        for a in spath:
            print(a.GetResidue().GetAtomID(a))
        print("End path")

    if not success:
        print("Error: Could not find all neighbours in the residue!")
        print("Residue number:", start.GetResidue().GetNum())
        print("Atom name:", start.GetResidue().GetAtomID(start))
        sys.exit(1)

    # Get path in the ending residue
    epath = []
    success = getNbrInRes(end, 1, path=epath, visited=[])

    if options.verbose:
        print("Start path", len(epath))
        for a in epath:
            print(a.GetResidue().GetAtomID(a))
        print("End path")

    if not success:
        print("Error: Could not find all neighbours in the residue!")
        print("Residue number:", end.GetResidue().GetNum())
        print("Atom name:", end.GetResidue().GetAtomID(end))
        sys.exit(1)

    # Combine the paths in both residues
    path = []
    for a in spath:
        path.append(a)
    path.append(start)
    path.append(end)
    for a in reversed(epath):
        path.append(a)

    if options.verbose:
        print("Start path", len(path))
        for a in path:
            print(a.GetResidue().GetAtomID(a))
        print("End path")

    # Construct conformation data structure
    conf = []
    conf.append(start.GetResidue().GetAtomID(start).strip())
    conf.append(end.GetResidue().GetAtomID(end).strip())
    for i in range(4):
        a = path[0+i]
        b = path[1+i]
        c = path[2+i]
        d = path[3+i]
        conf.append(a.GetResidue().GetAtomID(a).strip())
        conf.append(b.GetResidue().GetAtomID(b).strip())
        conf.append(c.GetResidue().GetAtomID(c).strip())
        conf.append(d.GetResidue().GetAtomID(d).strip())
        conf.append(mol.GetTorsion(a, b, c, d))

    return conf


def getResidueSMILES(mol):
    """Get canonical SMILES for residue fom prepReader.

    """
    builder = ob.OBBuilder()
    builder.SetKeepRings()

    # Find double bonds
    mol.BeginModify()
    for a in ob.OBMolAtomIter(mol):
        if a.GetImplicitValence() - a.GetValence() == 1:
            for b in ob.OBAtomAtomIter(a):
                if b.GetImplicitValence() - b.GetValence() == 1:
                    bond = mol.GetBond(a, b)
                    bond.SetBondOrder(bond.GetBondOrder()+1)
    mol.EndModify()

    # Add dummy atoms to fill the remaining free valences
    for a in ob.OBMolAtomIter(mol):
        if a.GetImplicitValence() - a.GetValence() == 1:
            d = mol.NewAtom()
            d.SetAtomicNum(0)
            builder.Connect(mol, a.GetIdx(), d.GetIdx())

    # Run the molecule through the PDB format plugin to normalize double bonds
    # and to get the correct canonical SMILES
    obc = ob.OBConversion()
    obc.SetInAndOutFormats("pdb", "pdb")

    mol2 = ob.OBMol()
    obc.ReadString(mol2, obc.WriteString(mol))

    # Convert to canonical SMILES with explicit hydrogens
    obc2 = ob.OBConversion()
    obc2.SetOutFormat("can")
    obc2.AddOption('h', obc2.OUTOPTIONS)

    return obc2.WriteString(mol2).split()[0]


def getGlycamName(options, mol, residue, vertex):
    """Get the Glycam name for given residue.

    """
    m = ob.OBMol()
    m.BeginModify()

    # Add residue atoms
    for a in ob.OBResidueAtomIter(residue):
        b = m.NewAtom()
        b.SetAtomicNum(a.GetAtomicNum())
        b.SetVector(a.GetVector())
        a.shadow = str(b.GetIdx())

    # Add dummy atoms
    for a in ob.OBResidueAtomIter(residue):
        for b in ob.OBAtomAtomIter(a):
            if residue.GetNum() != b.GetResidue().GetNum():
                c = m.NewAtom()
                c.SetAtomicNum(0)
                c.SetVector(b.GetVector())
                b.shadow = str(c.GetIdx())

    # Add bonds
    for a in ob.OBResidueAtomIter(residue):
        for b in ob.OBAtomBondIter(a):
            c = ob.OBBond()
            c.SetBegin(m.GetAtom(int(b.GetBeginAtom().shadow)))
            c.SetEnd(m.GetAtom(int(b.GetEndAtom().shadow)))
            c.SetBondOrder(b.GetBondOrder())
            m.AddBond(c)

    m.EndModify()

    # Add hydrogens
    m.AddHydrogens()

    # Get canonical SMILES
    obc = ob.OBConversion()
    obc.SetOutFormat("can")
    obc.AddOption('h', obc.OUTOPTIONS)

    smiles = obc.WriteString(m).strip()

    # Build dictionary of residue names only once
    if not hasattr(getGlycamName, 'residue_names'):
        residues = {}
        for i in options.prepFNs:
            print(i)
            with open(i, 'r') as f:
                ires = readPrepFile(f, True)
            for k, v in ires.items():
                residues[k] = v

        # Build dictionary of residue names keyed by SMILES
        getGlycamName.residue_names = {}
        for k, v in residues.items():
            s2 = getResidueSMILES(v)
            if s2 not in getGlycamName.residue_names:
                getGlycamName.residue_names[s2] = k
            else:
                print("Error: Duplicate SMILES found!")
                print(getGlycamName.residue_names[s2], s2)
                print(k, s2)

                obc2 = ob.OBConversion()
                obc2.SetOutFormat("pdb")

                r1 = getGlycamName.residue_names[s2]
                fn = "error_{}.pdb".format(r1)
                with open(fn, 'w') as f:
                    f.write(obc2.WriteString(residues[r1]))

                r2 = k
                fn = "error_{}.pdb".format(r2)
                with open(fn, 'w') as f:
                    f.write(obc2.WriteString(v))

    try:
        return getGlycamName.residue_names[smiles]
    except KeyError:
        s = "Error: Could not find residue {} in PREP file(s)!"
        print(s.format(vertex.name + str(vertex.num)))
        return None


def buildGraph(options, graph, vertex, mol, residue, visited=[]):
    """Build a tree graph staring from the given vertex.

    """
    if options.prepFNs:
        vertex.glycam_name = getGlycamName(options, mol, residue, vertex)
    resn = residue.GetNum()
    visited.append(resn)
    for a in ob.OBResidueAtomIter(residue):
        for b in ob.OBAtomAtomIter(a):
            resn2 = b.GetResidue().GetNum()
            if (resn2 != resn
                and resn2 not in visited):
                residue2 = b.GetResidue()

                parent = vertex
                num    = residue2.GetNum()
                name   = residue2.GetName()

                v = Vertex(parent, num, name)

                graph.add_vertex(v)
                graph.add_edge(Edge(vertex, v, getConf(options, mol, a, b)))

                buildGraph(options, graph, v, mol, residue2, visited)


def findAllPaths(vertex, paths, path=[]):
    """Find all paths from a given vertex to the leaf nodes.

    """
    if not vertex.branches:
        paths.append(path)
        return

    for v in vertex.branches:
        findAllPaths(v, paths, path[:] + [v])


def getLongestPath(vertex):
    """Find the longest path from a given vertex to the leaf node.

    """
    paths = []

    findAllPaths(vertex, paths, [vertex])

    longest = paths[0]
    for path in paths:
        if len(path) > len(longest):
            longest = path

    return longest


def buildBranches(options, branches, thrunk, n=0, visited=[]):
    """Build the list of branches on the given thrunk.

    Add branch_id to the vertices and a branch_tag to the first vertex of a
    branch.
    """
    if thrunk[0].parent is None:
        thrunk[0].branch_tag = 'branch' + str(n)

    for i, v in enumerate(thrunk):
        if thrunk[0].parent is None:
            if i > 0:
                v.branch_id = 'branch{}.{}'.format(n, i)
        else:
            v.branch_id = 'branch{}.{}'.format(n, i+1)

    for i, v in enumerate(thrunk):
        for w in v.branches:
            if options.verbose:
                print("v1", v)
                print("v2", w)

            if w == thrunk[i+1] or w in visited:
                if options.verbose:
                    print("Skip", w)
                continue

            visited.append(w)
            branch = getLongestPath(w)
            n += 1
            w.branch_tag = 'branch' + str(n)
            v.tags.append(w.branch_tag)
            branches.append(branch)
            n = buildBranches(options, branches, branch, n, visited)

    return n


def writeBranch(graph, branch, edges, u, c):
    """Write the given branch excluding root.

    """
    for i, v in enumerate(branch):
        if branch[0].parent is None and i < 2:
            continue
        elif i < 1:
            continue
        e = graph.get_edge(branch[i-1], branch[i])
        edges.append(e)
        if v.glycam_name is None:
            rname = v.name + str(v.num)
        else:
            rname = v.glycam_name
        s = "-({},{},<conf{}>)-{}"
        u[-1] += s.format(e.conf[0], e.conf[1], c, rname)
        if v.tags:
            s = "<{}>"
            t = ','.join(v.tags)
            u[-1] += s.format(t)
        c += 1
    u[-1] += '\n\n'

    return c


def writeConf(edges, u, cstart):
    """Write conformations for the given edges.

    """
    for c, e in enumerate(edges, start=cstart):
        s1 = "conf{}={},{},["
        u.append(s1.format(c, e.conf[0], e.conf[1]))
        t = []
        s2 = "{} {} {} {} {: 6.1f}"
        for i in range(4):
            t.append(s2.format(e.conf[2+5*i],
                               e.conf[3+5*i],
                               e.conf[4+5*i],
                               e.conf[5+5*i],
                               e.conf[6+5*i]))
        u[-1] += ' '.join(t)
        u[-1] += "]\n"
    u[-1] += "\n"


def writeBranches(options, graph, branches, u, c=0, n=0):
    """Recursively write sequences for the branches.

    """
    # Write branch
    b = branches[n]
    cstart = c

    if options.verbose:
        print("Start branch", len(b))
        for v in b:
            print(v)
        print("End branch")

    e = graph.get_edge(b[0].parent, b[0])
    edges = [e]

    s = "{}={}:-({},{},<conf{}>)-{}"
    if b[0].glycam_name is None:
        rname = b[0].name + str(b[0].num)
    else:
        rname = b[0].glycam_name
    u.append(s.format(b[0].branch_tag,
                      b[0].parent.branch_id,
                      e.conf[0],
                      e.conf[1],
                      c,
                      rname))
    if b[0].tags:
        s = "<{}>"
        t = ','.join(b[0].tags)
        u[-1] += s.format(t)
    c += 1

    c = writeBranch(graph, b, edges, u, c)

    writeConf(edges, u, cstart)

    if len(branches) > n+1:
        c = writeBranches(options, graph, branches, u, c, n+1)

    return c


def writeTree(options, graph, branches, u, c=0, n=0):
    """Write sequences for the given tree.

    """
    # Write thrunk
    b = branches[n]
    cstart = c

    if options.verbose:
        print("Start thrunk", len(b))
        for v in b:
            print(v)
        print("End thrunk")

    e = graph.get_edge(b[0], b[1])
    edges = [e]

    s = "{}=P.{}:-({},{},<conf{}>)-{}"
    if b[1].glycam_name is None:
        rname = b[1].name + str(b[1].num)
    else:
        rname = b[1].glycam_name
    u.append(s.format(b[0].branch_tag,
                      b[0].num,
                      e.conf[0],
                      e.conf[1],
                      c,
                      rname))
    if b[1].tags:
        s = "<{}>"
        t = ','.join(b[1].tags)
        u[-1] += s.format(t)
    c += 1

    c = writeBranch(graph, b, edges, u, c)

    writeConf(edges, u, cstart)

    # Write branches recursively
    if len(branches) > n+1:
        c = writeBranches(options, graph, branches, u, c, n+1)

    return c



if __name__ == '__main__':
    options, args = optP()

    # Read the molecule
    filename = options.inFN
    fname    = os.path.basename(filename)
    fname    = os.path.splitext(fname)[0]
    fformat  = os.path.splitext(filename)[1][1:]

    pybmol = next(pybel.readfile(fformat, filename))

    mol = pybmol.OBMol
    if options.verbose:
        print("Title:           %s" % pybmol.title)
        print("MW:              %f" % pybmol.molwt)
        print("Number of atoms: %i" % mol.NumAtoms())

    roots = []
    if not isinstance(options.roots[0], int):
        # Auto-discover the attachment points
        for r in ob.OBResidueIter(mol):
            if r.GetName() not in options.roots[::2]:
                continue
            for a in ob.OBResidueAtomIter(r):
                for b in ob.OBAtomAtomIter(a):
                    if b.GetResidue().GetName() in options.roots[1::2]:
                        roots.extend([a.GetIdx(), b.GetIdx()])
    else:
        # Attachment points are specified on the command line
        roots = options.roots

    branchn = 0
    confn   = 0
    u       = []

    # Generate sequences
    for a, b in zip(roots[::2], roots[1::2]):
        start = mol.GetAtom(a)
        end   = mol.GetAtom(b)

        sres = start.GetResidue()
        eres = end.GetResidue()

        conf = getConf(options, mol, start, end)

        # Create the root node and the node for the first sugar
        g = Graph()

        parent = None
        num    = sres.GetNum()
        name   = sres.GetName()

        v1 = Vertex(parent, num, name)

        g.add_vertex(v1)

        parent = v1
        num    = eres.GetNum()
        name   = eres.GetName()

        v2 = Vertex(parent, num, name)

        g.add_vertex(v2)

        g.add_edge(Edge(v1, v2, conf))

        # Do DFS to build the tree of sugar residues
        buildGraph(options, g, v2, mol, eres, visited=[sres.GetNum()])

        # Find the longest path from root
        thrunk = getLongestPath(v1)

        # Build the thrunk and the branches as separate, linear trees
        branches = [thrunk]
        branchn = buildBranches(options, branches, thrunk, branchn)
        branchn += 1

        # Write out the branches
        confn = writeTree(options, g, branches, u, confn)

    with open(options.outFN, 'w') as f:
        f.writelines(u)
