#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Version 0.03
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Ring conformer generator.
"""

from __future__ import print_function
from __future__ import unicode_literals

import sys
import numpy as np
import pybel
import os.path
import openbabel as ob

from scanCon import GamConf
from optparse import OptionParser



def optP():
    """Create and initialize Option Parser.

    """
    usage = '[python] %prog [-cv] [-r i1 i2 i3 i4 i5 i6] [-k N] input_file'
    version = '\n%prog Version 0.02\n\nRequires Python 2.6 ' \
              'or newer.'
    description = 'Ring puckering coordinate calculator and conformer ' \
                  'generator. Now implemented only for 6-member rings.'
    optParser = OptionParser(usage=usage,
                             version=version,
                             description=description)

    optParser.set_defaults(ring=None, step=10, calc_only=False, verbose=False,
                           set_puck=False, Q=None, theta=None, phi=None,
                           dlc=False, quanta=None, charge=0, mult=1,
                           scan_axis=None, relaxed=False)

    optParser.add_option('-v', action='store_true',
                         dest='verbose',
                         help='Be verbose [default: %default]')
    optParser.add_option('-c', action='store_true',
                         dest='calc_only',
                         help='Calculate puckering coordinates for the '
                         'input structure [default: %default]')
    optParser.add_option('-r', action='callback',
                         callback=getParams,
                         dest='ring',
                         help='Ring to pucker [default: %default]')
    optParser.add_option('-k', type='int',
                         dest='step',
                         help='Step size in coordinate scan (degrees) '
                         '[default: %default]')

    optParser.add_option('-s', action='store_true',
                         dest='set_puck',
                         help='Set puckering coordinates for the '
                         'input structure [default: %default]')
    optParser.add_option('-Q', type='float',
                         dest='Q',
                         help='Value of total puckering amplitude Q (Å) '
                         ' [default: %default]')
    optParser.add_option('--theta', action='callback',
                         callback=getParams,
                         dest='theta',
                         help='Value(s) of angle(s) θ (deg) '
                         '[default: %default]')
    optParser.add_option('--phi', action='callback',
                         callback=getParams,
                         dest='phi',
                         help='Value(s) of angle(s) ϕ (deg) '
                         '[default: %default]')

    optParser.add_option('-a', type='int', nargs=2,
                         dest='scan_axis',
                         help='Indexes (0-based) of phi and theta to scan '
                         '[default: %default]')
    optParser.add_option('-R', action='store_true',
                         dest='relaxed',
                         help='Generate QM input files for relaxed Q '
                         '[default: %default]')

    optParser.add_option('--dlc', action='store_true',
                         dest='dlc',
                         help='Use internal coordinates in GAMESS '
                         '[default: %default]')
    optParser.add_option('-L', type='int',
                         dest='quanta',
                         help='Level of QM optimizations [default: %default]')
    optParser.add_option('-C', type='int',
                         dest='charge',
                         help='Charge of the molecule [default: %default]')
    optParser.add_option('-M', type='int',
                         dest='mult',
                         help='Multiplicity of the molecule '
                         '[default: %default]')
    optParser.add_option('-w', action='store_true',
                         dest='solw',
                         help='Use implicit solvent (water) '
                         '[default: %default]')

    options, args = optParser.parse_args()

    options.ring = tuple(options.ring)

    if options.ring is None:
        print(usage)
        print("Error: Use flag -r to specify ring!")
        sys.exit(1)

    if options.scan_axis is not None:
        if len(options.ring) < 6:
            s = "Error: phi and theta is defined only for ring sizes > 5!"
            print(s)
            sys.exit(1)
        elif (options.scan_axis[0] < 0 or
              options.scan_axis[0] >= np.ceil((len(options.ring)-4)/2)):
            s = "Error: Expected phi index in the range [0; %d], but got %d!"
            print(s % (np.ceil((len(options.ring)-4)/2)-1,
                       options.scan_axis[0]))
            sys.exit(1)
        elif (options.scan_axis[1] < 0 or
              options.scan_axis[1] >= np.floor((len(options.ring)-4)/2)):
            s = "Error: Expected theta index in the range [0; %d], but got %d!"
            print(s % (np.floor((len(options.ring)-4)/2)-1,
                       options.scan_axis[1]))
            sys.exit(1)

    if (options.set_puck or
        options.scan_axis):
        if len(options.ring) < 4:
            s  = "Error: Puckering coordinates are defined only for rings of"
            s += " size N >= 4!"
            print(s)
            sys.exit(1)

        elif len(options.ring) == 4:
            if (options.phi   is not None or
                options.theta is not None):
                s = "Error: For ring size N = 4 only Q is defined!"
                print(s)
                sys.exit(1)
            elif options.Q is None:
                s = "Error: Total puckering amplitude is not provided!"
                print(s)
                sys.exit(1)

        elif len(options.ring) == 5:
            if options.theta is not None:
                s = "Error: For ring size N = 5 theta is not definet!"
                print(s)
                sys.exit(1)
            elif any(i is None for i in (options.Q,
                                         options.phi)):
                s = "Error: Not all of the puckering coordinates are provided!"
                print(s)
                sys.exit(1)
            elif len(options.phi) != 1:
                s = "Error: Expected 1 phi value, but got %d!"
                print(s % len(options.phi))
                sys.exit(1)

        else:
            if any(i is None for i in (options.Q,
                                       options.phi,
                                       options.theta)):
                s = "Error: Not all of the puckering coordinates are provided!"
                print(s)
                sys.exit(1)
            elif len(options.phi) != np.ceil((len(options.ring)-4)/2.0):
                s = "Error: Expected %d phi values, but got %d!"
                print(s % (np.ceil((len(options.ring)-4)/2.0),
                           len(options.phi)))
                sys.exit(1)
            elif len(options.theta) != np.floor((len(options.ring)-4)/2.0):
                s = "Error: Expected %d theta values, but got %d!"
                print(s % (np.floor((len(options.ring)-4)/2.0),
                           len(options.theta)))
                sys.exit(1)
            else:
                n = 0
                if len(options.ring) % 2 == 0:
                    if (options.theta[0] > 180.0 or
                        options.theta[0] < 0.0):
                        s = "Error: theta_1 is defined only for [0; 180]!"
                        print(s)
                        sys.exit(1)
                    elif (abs(options.theta[0] - 180) < 1 or
                          abs(options.theta[0]) < 1):
                        s = "Warnig: theta is not well behaved near the pols!"
                        print(s)
                    n = 1
                if (any(options.theta[n:] > 90.0) or
                    any(options.theta[n:] < 0.0)):
                    s = "Error: theta_n is defined only for [0; 90]!"
                    print(s)
                    sys.exit(1)
                elif (any(abs(options.theta[n:] - 90) < 1) or
                      any(abs(options.theta[n:]) < 1)):
                    s = "Warnig: theta is not well behaved near the pols!"
                    print(s)

    return options, args


def isNumber(s):
    try:
        float(s)
        return True
    except ValueError:
        return False


def getParams(option, opt_str, value, parser):
    """Extract specified number of parameters from command line.

    """
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if ((arg[:2] == '--' and len(arg) >= 2) or
            (arg[:1] == '-' and len(arg) >= 1 and not isNumber(arg)) or
            (not isNumber(arg))):
            break
        else:
            value.append(int(float(arg)))
            del rargs[0]
    setattr(parser.values, option.dest, np.array(value))


def getCoo(mol):
    """Create arrays of atom coordinates from OBMol.

    Return atom coordinates for the whole molecule and for the ring.
    """
    coo = [None for i in ring]
    cooa = []
    for i in ob.OBMolAtomIter(mol):
        x = i.x()
        y = i.y()
        z = i.z()
        cooa.append([x,y,z])
        if i.GetIdx() in ring:
            coo[ring.index(i.GetIdx())] = [x,y,z]

    return np.array(cooa), np.array(coo)


def updateCoo(coo, cooa, ring=()):
    """Update ring coordinates from the coordinates of molecule.

    """
    if not ring:
        raise Exception("No ring given!")

    for k, i in enumerate(cooa):
        if k+1 in ring:
            coo[ring.index(k+1)] = i


def showC(coo):
    """Print coordinates.

    """
    for k, i in enumerate(coo):
        print("%i  % f  % f  % f" % (k+1, i[0], i[1], i[2]))


def rotateXCooa(cooa, angle, ring=(), verbose=False):
    """Rotate molecule around x axis.

    """
    rotM = np.matrix([[1, 0, 0],
                      [0, np.cos(angle), -np.sin(angle)],
                      [0, np.sin(angle), np.cos(angle)]])

    if verbose:
        print("\nRotation angle: % 8.2f" % np.degrees(angle))
        print("\nRotation matrix:")
        print(rotM)
        print("Check rotation matrix...")
        print("R.T*R =")
        print(rotM.T*rotM)
        print("Det(R) = % f" % np.linalg.det(rotM))
        print("\nCoordinates:")
        s = "%5i  % f  % f  % f"

    for n, i in enumerate(cooa):
        cooa[n] = np.matrix(i)*rotM
        if verbose and n+1 in ring:
            print(s % (n+1, cooa[n][0], cooa[n][1], cooa[n][2]))


def rotateYCooa(cooa, angle, ring=(), verbose=False):
    """Rotate molecule around y axis.

    """
    rotM = np.matrix([[np.cos(angle), 0, np.sin(angle)],
                      [0, 1, 0],
                      [-np.sin(angle), 0, np.cos(angle)]])

    if verbose:
        print("\nRotation angle: % 8.2f" % np.degrees(angle))
        print("\nRotation matrix:")
        print(rotM)
        print("Check rotation matrix...")
        print("R.T*R =")
        print(rotM.T*rotM)
        print("Det(R) = % f" % np.linalg.det(rotM))
        print("\nCoordinates:")
        s = "%5i  % f  % f  % f"

    for n, i in enumerate(cooa):
        cooa[n] = np.matrix(i)*rotM
        if verbose and n+1 in ring:
            print(s % (n+1, cooa[n][0], cooa[n][1], cooa[n][2]))


def rotateZCooa(cooa, angle, ring=(), verbose=False):
    """Rotate molecule around z axis.

    """
    rotM = np.matrix([[np.cos(angle), -np.sin(angle), 0],
                      [np.sin(angle), np.cos(angle), 0],
                      [0, 0, 1]])

    if verbose:
        print("\nRotation angle: % 8.2f" % np.degrees(angle))
        print("\nRotation matrix:")
        print(rotM)
        print("Check rotation matrix...")
        print("R.T*R =")
        print(rotM.T*rotM)
        print("Det(R) = % f" % np.linalg.det(rotM))
        print("\nCoordinates:")
        s = "%5i  % f  % f  % f"

    for n, i in enumerate(cooa):
        cooa[n] = np.matrix(i)*rotM
        if verbose and n+1 in ring:
            print(s % (n+1, cooa[n][0], cooa[n][1], cooa[n][2]))


def checkMeanPlane(coo, rp, rpp, n, verbose=False):
    """Check the mean plane of the ring.

    This works only for 6-member rings.
    """
    if len(coo) != 6:
        raise Exception("Only 6-member rings are supported!")

    rp2  = (np.sqrt(3) / 2) * ((coo[1] + coo[2]) - (coo[4] + coo[5]))
    rpp2 = coo[0] + (coo[1] - coo[2]) / 2 - coo[3] - (coo[4] - coo[5]) / 2

    if verbose:
        print("R'  =",  rp)
        print("R\"  =", rpp)
        print("Check (hexagon)...")
        print("R'  =",  rp2)
        print("R\"  =", rpp2)
        print("n =", n)

    if (all(abs(j - i) < 1e-12 for i, j in zip(rp,  rp2)) and
        all(abs(j - i) < 1e-12 for i, j in zip(rpp, rpp2))):
        return True
    else:
        return False


def findMeanPlane(coo, verbose=False):
    """Find the normal to the mean plane.

    """
    rp  = np.zeros(3)
    rpp = np.zeros(3)

    for n, i in enumerate(coo):
        rp  += i*np.sin(2*np.pi*n/len(coo))
        rpp += i*np.cos(2*np.pi*n/len(coo))

    crp = np.cross(rp, rpp)
    n   = crp / np.sqrt(crp[0]**2 + crp[1]**2 + crp[2]**2)

    if len(coo) == 6 and not checkMeanPlane(coo, rp, rpp, n, verbose=verbose):
        print("\nError: Mean plane check failed!")
        checkMeanPlane(coo, rp, rpp, n, verbose=True)
        raise Exception("Mean plane check failed!")

    return n


def checkZj(zj, verbose=False):
    """Check displacements zj.

    """
    szc = 0.0
    szs = 0.0

    for k, i in enumerate(zj):
        szc += i*np.cos(2*np.pi*k/len(zj))
        szs += i*np.sin(2*np.pi*k/len(zj))

    if verbose:
        print("Sum(zⱼ) = % f" % sum(zj))
        print("szc = % f" % szc)
        print("szs = % f" % szs)

    if (abs(sum(zj)) < 1e-12 and
        abs(szc) < 1e-12 and
        abs(szs) < 1e-12):
        return True
    else:
        return False


def checkQm(qm, N):
    """Check qm.

    """
    if (N % 2 == 0):
        k = len(qm) - 1
    else:
        k = len(qm)

    for i in range(k):
        if qm[i] < -1e-12:
            return False

    return True


def findPuc(zj, verbose=False):
    """Calculate puckering coordinates qm, phi.

    """
    N  = len(zj)

    if N % 2 == 0:
        k = int((N-2)/2 + 1)
    else:
        k = int((N-1)/2 + 1)

    ss = [0.0 for i in range(2, k)]
    sc = [0.0 for i in range(2, k)]
    fi = [0.0 for i in range(2, k)]
    qm = [0.0 for i in range(2, k)]

    for m in range(2, k):
        for n, i in enumerate(zj):
            ss[m-2] += i*np.sin(m*2*np.pi*n/N)
            sc[m-2] += i*np.cos(m*2*np.pi*n/N)

        if (abs(ss[m-2]) < 1e-12 and
            abs(sc[m-2]) < 1e-12):
            fi[m-2] = 0.0
        else:
            fi[m-2] = np.arctan2(-ss[m-2], sc[m-2])

        if fi[m-2] < -1e-12:
            if verbose:
                print("\nShifting negative ϕ_%d! %f → %f" % (m,
                                                             fi[m-2],
                                                             fi[m-2]+np.pi*2))
            fi[m-2] += 2*np.pi

        qm[m-2] = np.sqrt(2.0/N)*np.sqrt(ss[m-2]**2 + sc[m-2]**2)

    if N % 2 == 0:
        s3 = 0.0
        for n, i in enumerate(zj):
            s3 += i*(-1)**n

        qm.append(np.sqrt(1.0/N)*s3)

    if verbose:
        for m in range(2, k):
            print("ss_%d = % f" % (m, ss[m-2]))
            print("sc_%d = % f" % (m, sc[m-2]))
            print("ϕ_%d = % f rad = % .0f deg" % (m, fi[m-2], np.degrees(fi[m-2])))
            print("q_%d = % f Å" % (m, qm[m-2]))
        if N % 2 == 0:
            print("s3 = % f" % s3)
            print("q_%d = % f Å" % (k, qm[-1]))

    return qm, fi


def checkQ(zj, qm, Q, verbose=False):
    """Check total puckering amplitude Q.

    """
    szj = sum(i**2 for i in zj)
    sqm = sum(i**2 for i in qm)

    if verbose:
        print("∑zⱼ² = % f" % szj)
        print("∑qₘ² = % f" % sqm)
        print("Q²   = % f" % Q**2)

    if (abs(szj - sqm)  < 1e-12 and
        abs(sqm - Q**2) < 1e-12):
        return True
    else:
        return False


def findQ(zj, qm, verbose=False):
    """Calculate total puckering amplitude Q.

    """
    Q = np.sqrt(sum(i**2 for i in qm))

    if verbose:
        print("Q = % f Å" % Q)

    if checkQ(zj, qm, Q, verbose=verbose):
        return Q
    else:
        print("Error: Q check failed!")
        checkQ(zj, qm, Q, verbose=True)
        sys.exit(2)


def psin(angles):
    """Compute product of sines for the given angles.

    """
    p = 1

    for i in angles:
        p *= np.sin(i)

    return p


def checkTheta(qm, Q, theta, verbose=False):
    """Check theta.

    """
    if verbose:
        print("% f == % f" % (qm[0], Q*psin(theta)))
        for i in range(1, len(qm)-1):
            print("% f == % f" % (qm[i], Q*psin(theta[:-i])*np.cos(theta[-i])))
        print("% f == % f" % (qm[-1], Q*np.cos(theta[0])))

    if (abs(qm[0] - Q*psin(theta)) > 1e-12):
        return False

    for i in range(1, len(qm)-1):
        if (abs(qm[i] - Q*psin(theta[:-i])*np.cos(theta[-i])) > 1e-12):
            return False

    if (abs(qm[-1] - Q*np.cos(theta[0])) > 1e-12):
        return False

    return True


def findTheta(qm, Q, verbose=False):
    """Calculate theta for spherical representation of puckering.

    """
    k = len(qm) - 1

    theta = [0.0 for i in range(k)]

    for i in range(1, k+1):
        theta[i-1] = np.arctan2(np.sqrt(sum(j**2 for j in qm[:-i])), qm[-i])

    return theta


def updateMol(mol, cooa):
    """Update atom coordinates of the molecule from array.

    """
    for k, i in enumerate(ob.OBMolAtomIter(mol)):
        i.SetVector(cooa[k][0], cooa[k][1], cooa[k][2])


def checkCs(mol, ring):
    """Calculate geometric center.

    Return coordinates for the geometric center of the whole molecule
    and of the ring.
    """
    s  = np.zeros(3)
    sr = np.zeros(3)
    for i in ob.OBMolAtomIter(mol):
        s += np.array([i.x(), i.y(), i.z()]) / mol.NumAtoms()
        if i.GetIdx() in ring:
            sr += np.array([i.x(), i.y(), i.z()]) / len(ring)

    return s, sr


def checkC(coo, verbose=False):
    """Calculate geometric center.

    """
    s = np.zeros(3)
    for i in coo:
        s += np.array(i) / len(coo)
        if verbose:
            print("Sum: % f % f % f" % (s[0], s[1], s[2]))

    return s[0], s[1], s[2]


def translateCooa(cooa, sx, sy, sz, verbose=False):
    """Translate given coordinates to the center.

    """
    v = -np.array([sx, sy, sz])

    for k, i in enumerate(cooa):
        cooa[k] = i+v

    if verbose:
        print(v)


def setToCenter(cooa, coo, ring=(), verbose=False):
    """Move molecule to the geometrical center of the ring.

    """
    if verbose:
        print("Center:")
    sx, sy, sz = checkC(coo, verbose=verbose)

    if verbose:
        print("\nTranslate...")
    translateCooa(cooa, sx, sy, sz, verbose=verbose)

    updateCoo(coo, cooa, ring=ring)
    if verbose:
        print("\nCoordinates of ring atoms:")
        showC(coo)
        print("Check center...")
        checkC(coo, verbose=verbose)


def setToPlane(cooa, coo, ring=(), verbose=False):
    """Align normal of the mean plane with z axis.

    """
    if verbose:
        print("\nAlign n with z axis...")
    n     = findMeanPlane(coo, verbose=verbose)
    angle = np.arctan2(n[2], n[1]) - np.pi/2

    rotateXCooa(cooa, angle, ring=ring, verbose=verbose)
    updateCoo(coo, cooa, ring=ring)
    checkC(coo, verbose=verbose)
    n     = findMeanPlane(coo, verbose=verbose)
    angle = np.arctan2(n[0], n[2])

    rotateYCooa(cooa, angle, ring=ring, verbose=verbose)
    updateCoo(coo, cooa, ring=ring)
    checkC(coo, verbose=verbose)
    n     = findMeanPlane(coo, verbose=verbose)
    angle = np.arctan2(coo[0][1], coo[0][0]) - np.pi/2

    rotateZCooa(cooa, angle, ring=ring, verbose=verbose)
    updateCoo(coo, cooa, ring=ring)
    checkC(coo, verbose=verbose)
    n = findMeanPlane(coo, verbose=verbose)


def getPuc(coo, verbose=False):
    """Calculate puckering coordinates of the ring.

    """
    if verbose:
        print("\nFind the mean plane...")
    n = findMeanPlane(coo, verbose=verbose)

    if verbose:
        print("\nCalculate zⱼ...")
    zj = [np.dot(i, n) for i in coo]

    if verbose:
        print(zj)
        print("Check zⱼ...")

    if checkZj(zj, verbose=verbose):
        if verbose:
            print("\nCalculate q and ϕ...")
        qm, fi = findPuc(zj, verbose=verbose)
        return zj, qm, fi
    else:
        print("Error: zⱼ check failed!")
        checkZj(zj, verbose=True)
        sys.exit(2)


def getZjs(qm, fi, ring=()):
    """Calculate atom displacements for given puckering.

    """
    N = len(ring)

    zjs = [0.0 for i in range(N)]

    for k in range(N):
        for m in range(2, len(fi)+2):
            zjs[k] += np.sqrt(2.0/N)*qm[m-2]*np.cos(fi[m-2] + (m*2*np.pi*k/N))
        if (N % 2 == 0):
            zjs[k] += np.sqrt(1.0/N)*qm[-1]*(-1)**k

    return zjs


def makePuc(mol, qm, fi, f, options, ring=(), verbose=False):
    """Generate ring conformer with given puckering.

    """
    if not checkQm(qm, len(ring)):
        print("Error: qm must be positive! qm = ",
              qm[:-1] if (len(ring) % 2 == 0) else qm)
        sys.exit(2)

    zjs   = getZjs(qm, fi, ring)
    Q     = findQ(zjs, qm, verbose=True)
    theta = findTheta(qm, Q, verbose=True)

    if verbose:
        print(zjs)

    for k, i in enumerate(coo):
        coo[k][2] = zjs[k]

    for k, i in enumerate(ring):
        cooa[i-1] = coo[k]

    for k, i in enumerate(ob.OBMolAtomIter(mol)):
        i.SetVector(cooa[k][0], cooa[k][1], cooa[k][2])

    pybmol.write('xyz', 'u%s.xyz' % f, overwrite=True)

    constr = ob.OBFFConstraints()
    for i in ring:
        constr.AddAtomZConstraint(i)

    obff = ob.OBForceField.FindForceField('MMFF94')
    print("Set up constraints: %s" % obff.Setup(mol, constr))
    print("E = % f" % obff.Energy())
    obff.SteepestDescent(2000)
    energy = obff.Energy()
    print("E = % f" % energy)
    mol.SetTitle("%s E=% f %s" % (f, energy, obff.GetUnit()))

    obff.UpdateCoordinates(mol)
    cooai, cooi = getCoo(mol)

    setToCenter(cooai, cooi, ring=ring, verbose=verbose)
    setToPlane(cooai,  cooi, ring=ring, verbose=verbose)

    zji, qmi, fii = getPuc(cooi, verbose=verbose)

    checkC(cooi, verbose=verbose)
    if verbose:
        print(ring)
        print(zjs)
        print(zji)
    for i in range(len(fi)):
        print("Needed: q_%d = % f, ϕ_%d = % f" % (i+2,
                                                  qm[i],
                                                  i+2,
                                                  np.degrees(fi[i])))
        print("Got:    q_%d = % f, ϕ_%d = % f" % (i+2,
                                                  qmi[i],
                                                  i+2,
                                                  np.degrees(fii[i])))
    if (len(ring) % 2 == 0):
        print("Needed: q_N/2 = % f" % (qm[-1]))
        print("Got:    q_N/2 = % f" % (qmi[-1]))
    if any(abs(i - j) > 1e-12 for i, j in zip(fi, fii)):
        print("Error: Got wrong conformation!")
        print(qmi, np.degrees(fii))
        pybmol.write('xyz', "%s_err.xyz" % f, overwrite=True)
        sys.exit(2)
    pybmol.write('xyz', "%s.xyz" % f, overwrite=True)

    if options.quanta is not None:
        writeGamIn(mol, "%s.inp" % f, options, ring, fi=fi, theta=theta)

    return energy


def makePuQ(mol, fi, theta, Q, filename, options, ring=(), verbose=False):
    """Generate ring conformer with given puckering.

    Use spherical coordinates.
    """
    for n, i in enumerate(fi):
        print("ϕ_%d = % f rad = % .0f deg" % (n+2, i, np.degrees(i)))
    for n, i in enumerate(theta):
        print("θ_%d = % f rad = % .0f deg" % (n+1, i, np.degrees(i)))
    print("Q   = % f Å" % Q)

    qm = [0.0 for i in range(len(theta)+1)]

    qm[0] = Q*psin(theta)
    for i in range(1,len(qm)-1):
        qm[i] = Q*psin(theta[:-i])*np.cos(theta[-i])
    qm[-1] = Q*np.cos(theta[0])

    return makePuc(mol, qm, fi, filename, options,
                   ring=ring, verbose=verbose)


def writeGamIn(mol, finp, options, ring=(), fi=None, theta=None):
    """Write GAMESS input file.

    """
    if not ring:
        raise Exception("Error: No ring provided!")

    o = GamConf()
    o.set('CONTRL', 'ICHARG', options.charge)
    o.set('CONTRL', 'MULT',   options.mult)
    o.set('CONTRL', 'RUNTYP', 'OPTIMIZE')
    o.set('CONTRL', 'EXETYP', 'RUN')
    o.set('CONTRL', 'MAXIT',  200)
    o.set('CONTRL', 'NZVAR',  3*mol.NumAtoms()-6)

    o.set('SCF', 'CONV',   '1.0E-08')
    o.set('SCF', 'DIRSCF', '.T.')

    if options.solw:
        o.set('PCM', 'SOLVNT', 'WATER')

    if options.quanta == 1:
        o.set('SYSTEM', 'TIMLIM', 180)
        o.set('SYSTEM', 'MWORDS', 15)
        o.set('BASIS',  'GBASIS', 'AM1')
    elif options.quanta == 2:
        o.set('CONTRL', 'DFTTYP', 'B3LYP')
        o.set('SYSTEM', 'TIMLIM', 1800)
        o.set('SYSTEM', 'MWORDS', 350)
        o.set('BASIS',  'GBASIS', 'N31')
        o.set('BASIS',  'NGAUSS', 6)
        o.set('BASIS',  'NDFUNC', 1)
    elif options.quanta == 3:
        o.set('SYSTEM', 'TIMLIM', 1800)
        o.set('SYSTEM', 'MWORDS', 350)
        o.set('BASIS',  'GBASIS', 'N31')
        o.set('BASIS',  'NGAUSS', 6)
        o.set('BASIS',  'NDFUNC', 1)
        o.set('BASIS',  'NPFUNC', 1)
        o.set('STATPT', 'HESS',   'CALC')
        o.set('STATPT', 'IHREP',  10)
    elif options.quanta == 4:
        o.set('CONTRL', 'MPLEVL', 2)
        o.set('SYSTEM', 'TIMLIM', 10080)
        o.set('SYSTEM', 'MWORDS', 350)
        o.set('SYSTEM', 'MEMDDI', 3000)

    o.set('STATPT', 'NSTEP', 2000)

    if not options.dlc:
        # Freeze Z position of ring atoms
        o.set('STATPT', 'IFREEZ(1)',
              ','.join('{}'.format(i*3) for i in sorted(ring)))
    else:
        # Freeze ring puckering coordinates
        o.set('ZMAT', 'DLC',  '.T.')
        o.set('ZMAT', 'AUTO', '.T.')

        sl = []
        sv = []

        if not options.relaxed:
            s  = '8,{},'.format(len(ring))
            s += ','.join('{}'.format(i) for i in ring)
            s += ',1'
            sl.append(s)
            sv.append('{:.4f}'.format(options.Q))

        for n, fi2 in fi:
            s  = '8,{},'.format(len(ring))
            s += ','.join('{}'.format(i) for i in ring)
            s += ',{}'.format(n+2)
            sl.append(s)
            sv.append('{:.0f}'.format(np.degrees(fi2)))

        for n, theta2 in theta:
            s  = '8,{},'.format(len(ring))
            s += ','.join('{}'.format(i) for i in ring)
            s += ',{}'.format(n+2+np.ceil((len(ring)-4)/2))
            sl.append(s)
            sv.append('{:.0f}'.format(np.degrees(theta2)))

        s = ', '.join(sl)
        o.set('ZMAT', 'IFZMAT(1)', s)
        s = ', '.join(sv)
        o.set('ZMAT', 'FVALUE(1)', s)

    mol.CloneData(o.make_gms_keys())
    converter = ob.OBConversion()
    converter.SetOutFormat('gamin')
    converter.SetOptions('k', converter.GENOPTIONS)
    converter.WriteFile(mol, finp)
    mol.DeleteData("gamess")



if __name__ == '__main__':
    options, args = optP()

    if all(not i for i in [options.calc_only, options.set_puck]):
        # Import plotting libraries only if necessary
        import matplotlib.pyplot as plt

        from matplotlib import cm
        from mpl_toolkits.mplot3d import Axes3D

    filename = args[0]
    fname    = os.path.basename(filename)
    fname    = os.path.splitext(fname)[0]
    fformat  = os.path.splitext(filename)[1][1:]

    pybmol = next(pybel.readfile(fformat, filename))

    mol = pybmol.OBMol
    if options.verbose:
        print("Title:           %s" % pybmol.title)
        print("Formula:         %s" % pybmol.formula)
        print("MW:              %f" % pybmol.molwt)
        print("Number of atoms: %i" % mol.NumAtoms())

    rings = mol.GetSSSR()
    if options.verbose:
        print("Molecule has %i ring(s) of size(s):" % len(rings))
        for i in rings:
            print(i.Size())

    # Select the ring
    ring = None
    for r in rings:
        if all(i in r._path for i in options.ring):
            ring = r._path
            break

    if ring is None:
        print("Error: Ring " + str(options.ring) + " not found!")
        sys.exit(1)

    # Roll the ring to beginning
    r = np.array(ring)
    r = np.roll(r, -ring.index(options.ring[0]))

    if r[1] != options.ring[1]:
        # Flip the ring
        r[1::] = r[:0:-1]
    ring = tuple(int(i) for i in r)

    # Check that the expected atom order was given
    if ring != options.ring:
        print("Found rings in molecule:")
        for r in rings:
            print(r._path)
        print("\nError: Incorrect ring atom order given!")
        print("\nExpected:", ring)
        print("Got:     ", options.ring)
        sys.exit(1)

    if options.verbose:
        print("Atoms in rings:")
        print(ring)
        for i in ob.OBMolAtomIter(mol):
            print(i.GetIdx(), end=' ')
        print("\n\nCoordinates:")
        for i in ob.OBMolAtomIter(mol):
            print("%i  % f  % f  % f" % (i.GetIdx(), i.x(), i.y(), i.z()))
        print("\nCoordinates of ring atoms:")
        for i in ring:
            for j in ob.OBMolAtomIter(mol):
                if i == j.GetIdx():
                    print("%i  % f  % f  % f" % (i, j.x(), j.y(), j.z()))
        print("Check center...")
        s, sr = checkCs(mol, ring)
        print(s)
        print(sr)
        print("\nCenter molecule...")

    cooa, coo = getCoo(mol)
    setToCenter(cooa, coo, ring=ring, verbose=options.verbose)
    setToPlane(cooa,  coo, ring=ring, verbose=options.verbose)
    zj, qm, fi = getPuc(coo, verbose=options.verbose)

    if options.verbose:
        print("\nCalculate total puckering...")
    Q = findQ(zj, qm, verbose=options.verbose)

    if options.verbose:
        print("\nCalculate theta...")
    theta = findTheta(qm, Q, verbose=options.verbose)

    if options.calc_only:
        print(ring)
        for n, i in enumerate(qm):
            print("q_%d = % 12f Å" % (n+2, i))
        for n, i in enumerate(fi):
            print("ϕ_%d = % 12f deg" % (n+2, np.degrees(i)))
        print("Q   = % 12f Å" % Q)
        for n, i in enumerate(theta):
            print("θ_%d = % 12f deg" % (n+1, np.degrees(i)))
        sys.exit(0)

    if options.verbose:
        print("\nUpdate atom coordinates...")
    updateMol(mol, cooa)

    if options.set_puck:
        print("\nSetting puckering coordinates ...")
        s = fname
        for i in options.phi:
            s += "_%i" % i
        for i in options.theta:
            s += "_%i" % i
        s += "_%f" % options.Q
        energy = makePuQ(mol,
                         np.radians(options.phi),
                         np.radians(options.theta),
                         options.Q,
                         s,
                         options,
                         ring=ring, verbose=options.verbose)
        sys.exit(0)

    if options.scan_axis is not None:
        if (len(options.ring) % 2 == 0 and
            options.scan_axis[1] == 0):
            rtheta = 180
        else:
            rtheta = 90
        s = "\nGenerate conformers for (ϕ_%d, theta_%d)...\n"
        print(s % (options.scan_axis[0]+2, options.scan_axis[1]+1))
        E = []
        for k, fi2 in enumerate(range(0, 360, options.step)):
            E.append([])
            options.phi[options.scan_axis[0]] = fi2

            for theta2 in range(options.step, rtheta, options.step):
                options.theta[options.scan_axis[1]] = theta2
                s = fname
                for i in options.phi:
                    s += "_%i" % i
                for i in options.theta:
                    s += "_%i" % i
                s += "_%f" % options.Q
                energy = makePuQ(mol,
                                 np.radians(options.phi),
                                 np.radians(options.theta),
                                 options.Q,
                                 s,
                                 options,
                                 ring=ring, verbose=options.verbose)
                E[k].append(energy)

        fig = plt.figure()
        ax = Axes3D(fig)
        X = np.arange(options.step, rtheta, options.step)
        Y = np.arange(0.0,          360,    options.step)
        X, Y = np.meshgrid(X, Y)
        Z = np.array(E)
        ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet)
        plt.show()
