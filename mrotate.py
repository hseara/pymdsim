#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.02
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Set all matching dihedrals to given value. Atoms of the dihedral have to be
the first four matching atoms of given SMARTS.
"""

import os
import pybel
import openbabel as ob

from optparse import OptionParser
from math import pi


# Parse command line options.
usage="[python3] %prog [-B] -i file1 -o file2 -s smarts [-d value]"
description="Set all matching dihedrals to given value."
version="\n%prog Version 0.02\n\nRequires Python 3."
optParser = OptionParser(usage=usage,
                         version=version,
                         description=description)

optParser.set_defaults(inFN=None, outFN=None, smarts=None, value=180.0,
                       noBonds=False)
optParser.add_option('-i', type='str',
                     dest='inFN',
                     help="Input file name [default: %default]")
optParser.add_option('-o', type='str',
                     dest='outFN',
                     help="Output file name [default: %default]")
optParser.add_option('-s', type='str',
                     dest='smarts',
                     help="SMARTS string to match [default: %default]")
optParser.add_option('-d', type='float',
                     dest='value',
                     help="Dihedral value [default: %default]")
optParser.add_option('-B', action='store_true',
                     dest='noBonds',
                     help="Don't perceive bonds from atom distances"
                     " [default: %default]")

options, args = optParser.parse_args()


# Check if required parameters are given
if (options.inFN is None
    or options.outFN is None
    or options.smarts is None):
    optParser.error("Missing required option")


# Get input file name and format
fname = options.inFN
fformat = os.path.splitext(fname)[1][1:]

# Read input file
if not options.noBonds:
    # Read file with default settings
    pybmol = next(pybel.readfile(fformat, fname))

    mol = pybmol.OBMol
else:
    # Manually create OBConversion instance and adjust reading
    if fformat != 'pdb':
        print("Warning: Disabling bond perception!")
        print("This is known to work correctly only with PDB files"
              " with CONECT records.")

    mol = ob.OBMol()

    conv = ob.OBConversion()
    conv.SetInFormat(fformat)
    # Add option to disable bond perception
    # Relies on connection table for bonding information
    conv.AddOption('b', conv.INOPTIONS)
    conv.ReadFile(mol, fname)

    # Still need to perceive bond orders for SMARTS to work as expected
    mol.PerceiveBondOrders()

    pybmol = pybel.Molecule(mol)

# Set SMARTS pattern
smarts = pybel.Smarts(options.smarts)

# Set dihedral angle for every match
for match in smarts.findall(pybmol):
    a, b, c, d = [mol.GetAtom(i) for i in match[:4]]
    # Torsion angle value has to be converted to radians
    mol.SetTorsion(a, b, c, d, options.value * pi / 180.0)

# Write out molecule
offormat = os.path.splitext(options.outFN)[1][1:]
pybmol.write(offormat, options.outFN, overwrite=True)
