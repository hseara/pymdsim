#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.01
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Parse Amber and Gromacs log files to compare energies.
"""

import re
import sys
import os.path
import pybel
import openbabel as ob
from optparse import OptionParser


IGNORE_FRAMES = []


def optP():
    """Create and initialize Option Parser.

    """
    usage = '[python] %prog {-a amber_log | -e amber_en_log} -g gromacs_log'
    version = '\n%prog Version 0.01\n\nRequires Python 2.4 ' \
              'or newer.'
    description = 'Parse Amber and Gromacs log files to compare energies. ' \
                  'If Amber energy file is given (option -e), ' \
                  'then the initial frame (step 0) is omitted from ' \
                  'comparison since it is not written in the energy file. ' \
                  'If both -a and -e options are given, then only -e is used.'
    optParser = OptionParser(usage=usage,
                             version=version,
                             description=description)
    optParser.set_defaults(amberLog=None, amberEnLog=None, gromacsLog=None,
                           amberRerunLog=None, mdtrj=None, potOut=None,
                           discardFirst=False, discardFirst2=False,
                           relativeDiff=False, suppressWarnings=False,
                           gromacsMinLog=False, cluster=False, verbose=False)
    optParser.add_option('-1', action='store_true',
                         dest='discardFirst',
                         help='To discard the first frame [default: %default]')
    optParser.add_option('-2', action='store_true',
                         dest='discardFirst2',
                         help='To discard two first frames [default: %default]')
    optParser.add_option('-a', type='str',
                         dest='amberLog',
                         help='Amber log file name [default: %default]')
    optParser.add_option('-b', action='callback',
                         callback=getFileNames,
                         dest='amberRerunLog',
                         help='Amber rerun log file name [default: %default]')
    optParser.add_option('-e', type='str',
                         dest='amberEnLog',
                         help='Amber energy log file name [default: %default]')
    optParser.add_option('-g', action='callback',
                         callback=getFileNames,
                         dest='gromacsLog',
                         help='Gromacs log file name [default: %default]')
    optParser.add_option('-m', action='store_true',
                         dest='gromacsMinLog',
                         help='Gromacs log file (bond) [default: %default]')
    optParser.add_option('-f', type='str',
                         dest='mdtrj',
                         help='Trajectory (e.g., pdb) [default: %default]')
    optParser.add_option('-o', type='str',
                         dest='potOut',
                         help='Output file for potentials [default: %default]')
    optParser.add_option('-r', action='store_true',
                         dest='relativeDiff',
                         help='Output relative energy differences (%)'
                         ' [default: %default]')
    optParser.add_option('-c', action='store_true',
                         dest='cluster',
                         help='Average differences of conformations for each'
                         ' structure [default: %default]')
    optParser.add_option('-v', action='store_true',
                         dest='verbose',
                         help='Output every step [default: %default]')
    optParser.add_option('-W', action='store_true',
                         dest='suppressWarnings',
                         help='Suppress warnings [default: %default]')
    options, args = optParser.parse_args()
    return options, args

def getFileNames(option, opt_str, value, parser):
    """Extract specified number of parameters from command line.

    """
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if ((arg[:2] == '--' and len(arg) >= 2) or
            (arg[:1] == '-' and len(arg) >= 1)):
            break
        else:
            value.append(arg)
            del rargs[0]
    setattr(parser.values, option.dest, value)


def parseAmberEnLog(filename):
    kcal2kJ = 4.184
    f = open(filename, 'r')
    frames = []
    for i in range(11):
        line = f.readline()
    numbers = {'step': -1}
    frames.append(numbers)
    while line != '':
        line = line.split()
        numbers = {'step': int(line[1]),
                   'time': float(line[2])}
        for i in range(5):
            f.readline()
        line = f.readline().split()
        numbers['potential'] = float(line[2])*kcal2kJ
        numbers['LJ'] = float(line[3])*kcal2kJ
        numbers['C'] = float(line[4])*kcal2kJ
        line = f.readline().split()
        numbers['bond'] = float(line[2])*kcal2kJ
        numbers['angle'] = float(line[3])*kcal2kJ
        numbers['dihedral'] = float(line[4])*kcal2kJ
        line = f.readline().split()
        numbers['LJ-14'] = float(line[1])*kcal2kJ
        numbers['C-14'] = float(line[2])*kcal2kJ
        f.readline()
        frames.append(numbers)
        line = f.readline()
    f.close()
    return frames


def findAResults(f, fname):
    reXi = re.compile(r'^\s*[0-9][.]\s\sRESULTS$')
    f.seek(0)
    line = f.readline()
    while line != '':
        if reXi.match(line) is not None:
            # Results section found
            break
        line = f.readline()
    if line == '':
        print("Error: Amber log results section not found in", fname)
        sys.exit(1)


def findNextAFrame(f):
    reXk = re.compile(r'^\sNSTEP\s=\s*[0-9]*\s*')
    line = f.readline()
    while line != '':
        if reXk.match(line) is not None:
            break
        line = f.readline()
    if line == '':
        return -1, ''
    return int(line.split()[2]), line


def findNextARFrame(f):
    reXk = re.compile(r'^\s*NSTEP\s*')
    reXf = re.compile(r'^\s*FINAL\sRESULTS\s*')
    line = f.readline()
    while line != '' and reXf.match(line) is None:
        if reXk.match(line) is not None:
            break
        line = f.readline()
    if line == '' or reXf.match(line) is not None:
        return -1, ''
    return 0, line


def getAmberNumbers(f, line, frames):
    kcal2kJ = 4.184
    line = line.split()
    numbers = {'step': int(line[2]),
               'time': float(line[5])}
    line = f.readline().split()
    numbers['potential'] = float(line[8])*kcal2kJ
    line = f.readline().split()
    numbers['bond'] = float(line[2])*kcal2kJ
    numbers['angle'] = float(line[5])*kcal2kJ
    numbers['dihedral'] = float(line[8])*kcal2kJ
    line = f.readline().split()
    numbers['LJ-14'] = float(line[3])*kcal2kJ
    numbers['C-14'] = float(line[7])*kcal2kJ
    numbers['LJ'] = float(line[10])*kcal2kJ
    line = f.readline().split()
    numbers['C'] = float(line[2])*kcal2kJ
    frames.append(numbers)


def getARNumbers(f, line, nstep, frames, fname):
    N = -1+1
    if nstep == N:
        print(line)
    kcal2kJ = 4.184
    line = line.split()
    numbers = {'filename': fname,
               'step': nstep,
               'time': nstep*100.0}
    line = f.readline().split()
    if nstep == N:
        print(line)
    numbers['potential'] = float(line[1])*kcal2kJ
    f.readline()
    line = f.readline().split()
    if nstep == N:
        print(line)
    numbers['bond'] = float(line[2])*kcal2kJ
    numbers['angle'] = float(line[5])*kcal2kJ
    numbers['dihedral'] = float(line[8])*kcal2kJ
    line = f.readline().split()
    if nstep == N:
        print(line)
    try:
        numbers['LJ'] = float(line[2])*kcal2kJ
    except ValueError:
        print("Detected atom collisions in", fname)
        sys.exit(3)
    numbers['C'] = float(line[5])*kcal2kJ
    line = f.readline().split()
    if nstep == N:
        print(line)
    numbers['LJ-14'] = float(line[3])*kcal2kJ
    numbers['C-14'] = float(line[7])*kcal2kJ
    frames.append(numbers)


def printFrames(frames):
    for i in frames:
        print("Step = %10i Time = %10.5f" % (i['step'], i['time']))
        print("Bond = %1.5e Angle = %1.5e Dih. = %1.5e" % (i['bond'],
                                                           i['angle'],
                                                           i['dihedral']))
        print("LJ-14 = %1.5e Coulomb-14 = %1.5e" % (i['LJ-14'], i['C-14']))
        print("LJ = %1.5e Coulomb = %1.5e" % (i['LJ'], i['C']))
        print("Potential = %1.5e" % i['potential'])


def parseAmberLog(filename):
    nstepOld = -1
    frames = []
    f = open(filename, 'r')
    findAResults(f, filename)
    line = f.readline()
    while line != '':
        nstep, line = findNextAFrame(f)
        if nstep == -1:
            break
        if nstep == nstepOld:
            #print "The same step number as previously", nstep
            continue
        nstepOld = nstep
        getAmberNumbers(f, line, frames, filename)
    f.close()
    #printFrames(frames)
    return frames


def parseAmberRerunLog(filename):
    nstep = 0
    frames = []
    f = open(filename, 'r')
    findAResults(f, filename)
    line = f.readline()
    nstep1, line = findNextARFrame(f)
    while line != '':
        if nstep == -1:
            break
        nstep += 1
        getARNumbers(f, line, nstep, frames, filename)
        nstep1, line = findNextARFrame(f)
    f.close()
    #printFrames(frames)
    return frames


def parseAmberRerunLogFiles(filenames):
    frames = []
    for i in filenames:
        frames.extend(parseAmberRerunLog(i))
    return frames


def getGromacsNumbers(f, line, frames, fname):
    line = line.split()
    numbers = {'filename': fname,
               'step': int(line[0]),
               'time': float(line[1])}
    f.readline()
    line = f.readline()
    if line == '\n':
        line = f.readline()
    line = f.readline().split()
    if "Improper" in line:
        line = f.readline().split()
        numbers['bond'] = float(line[0])
        numbers['angle'] = float(line[1])
        numbers['dihedral'] = float(line[2]) + float(line[3])
        numbers['LJ-14'] = float(line[4])
        f.readline()
        line = f.readline().split()
        numbers['C-14'] = float(line[0])
        numbers['LJ'] = float(line[1])
        numbers['C'] = float(line[2])
        numbers['potential'] = float(line[3])
    else:
        line = f.readline().split()
        numbers['bond'] = float(line[0])
        numbers['angle'] = float(line[1])
        numbers['dihedral'] = float(line[2])
        numbers['LJ-14'] = float(line[3])
        numbers['C-14'] = float(line[4])
        f.readline()
        line = f.readline().split()
        numbers['LJ'] = float(line[0])
        numbers['C'] = float(line[1])
        numbers['potential'] = float(line[2])
    reX = re.compile(r'^\s*$')
    count = 0
    while line != '':
        line = f.readline()
        if reX.match(line) is not None:
            count += 1
        if count >= 2:
            break
    frames.append(numbers)


def getGMNumbers(f, line, frames, fname):
    line = line.split()
    numbers = {'filename': fname,
               'step': int(line[0]),
               'time': float(line[1])}
    f.readline()
    f.readline()
    f.readline()
    line = f.readline().split()
    numbers['bond'] = float(line[0])
    numbers['angle'] = 0.0
    numbers['dihedral'] = 0.0
    numbers['LJ-14'] = 0.0
    numbers['C-14'] = 0.0
    #f.readline()
    #line = f.readline().split()
    numbers['LJ'] = float(line[1])
    numbers['C'] = float(line[2])
    numbers['potential'] = float(line[3])
    for i in range(4):
        f.readline()
    frames.append(numbers)


def parseGromacsLog(filename):
    reXj = re.compile(r'^Started mdrun')
    reXl = re.compile(r'^\s*M\sE\sG\sA\s')
    frames = []
    f = open(filename, 'r')
    line = f.readline()
    while line != '':
        if reXj.match(line) is not None:
            break
        line = f.readline()
    f.readline()
    f.readline()
    line = f.readline()
    while line != '' and reXl.match(line) is None:
        getGromacsNumbers(f, line, frames, filename)
        line = f.readline()
    f.close()
    #printFrames(frames)
    return frames


def parseGromacsLogFiles(filenames):
    frames = []
    for i in filenames:
        frames.extend(parseGromacsLog(i))
    return frames


def getDiff(a, g, k, absolute=True):
    if abs(a[k]) < 1e-16 and abs(g[k]) < 1e-16:
        return 0.0
    elif abs(a[k]) < 1e-16:
        print("Warning: Amber %s, step %i: Eg=%.10f Ea=%.10f" % (k,
                                                                 g['step'],
                                                                 g[k],
                                                                 a[k]))
        return 0.0
    elif abs(g[k]) < 1e-16:
        print("Warning: Gromacs %s, step %i: Eg=%.10f Ea=%.10f" % (k,
                                                                   g['step'],
                                                                   g[k],
                                                                   a[k]))
        return 0.0
    else:
        if abs(a[k]-g[k]) < 1e-4 and not options.suppressWarnings:
            s = "Warning: absolute %s energy difference is less than 1e-4," % k
            s += "step %i: Eg=%.8f Ea=%.8f" % (g['step'], g[k], a[k])
            print(s)
            return 0.0
        if absolute:
            return g[k] - a[k]
        else:
            return g[k]*100/a[k] - 100


def cluster(framesAmber, framesGromacs):
    """Average over conformations of the same residues.

    """
    keys = ['bond', 'angle', 'dihedral',
            'LJ-14', 'C-14', 'LJ', 'C', 'potential',
            'step']
    am = {}
    gr = {}

    for a, g in zip(framesAmber, framesGromacs):
        if a['filename'][:-4] != g['filename'][:-4]:
            print("Error: Mismached filenames!")
            print("a:", a['filename'])
            print("g:", g['filename'])
            sys.exit(5)
        if a['filename'] is not None:
            molname = a['filename'].split('_')[0]
        else:
            continue

        # Collect
        ad = {k: [] for k in keys}
        gd = {k: [] for k in keys}
        ad['filename'] = molname
        gd['filename'] = molname
        for a1, g1 in zip(framesAmber, framesGromacs):
            if a1['filename'][:-4] != g1['filename'][:-4]:
                print("Error: Mismached filenames!")
                print("a:", a1['filename'])
                print("g:", g1['filename'])
                sys.exit(5)
            if a1['filename'] is not None:
                molname1 = a1['filename'].split('_')[0]
            else:
                continue
            if molname1 != molname:
                continue

            for k in keys:
                ad[k].append(a1[k])
                gd[k].append(g1[k])

        # Average
        for k in keys:
            ad[k] = sum(ad[k]) / len(ad[k])
            gd[k] = sum(gd[k]) / len(gd[k])

        # Save
        am[ad['filename']] = ad
        gr[gd['filename']] = gd

    return [am[k] for k in am.keys()], [gr[k] for k in am.keys()]


def compare(framesAmber, framesGromacs, absolute=True):
    print("Number of Amber frames:  ", len(framesAmber))
    print("Number of Gromacs frames:", len(framesGromacs))
    for a, g in zip(framesAmber, framesGromacs):
        if a['filename'][:-4] != g['filename'][:-4]:
            print("Error: Mismached filenames!")
            print("a:", a['filename'])
            print("g:", g['filename'])
            sys.exit(5)
        if a['filename'] is not None:
            print(a['filename'].split('.')[0])

        u = "(kJ)" if absolute else "(%) "
        print("Step %9i        delta %s    Gromacs         Amber" % (g['step'],u))
        d = getDiff(a, g, 'bond', absolute)
        print("Bond:       % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['bond'],
                                                       a['bond']))
        d = getDiff(a, g, 'angle', absolute)
        print("Angle:      % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['angle'],
                                                       a['angle']))
        d = getDiff(a, g, 'dihedral', absolute)
        print("Dih.:       % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['dihedral'],
                                                       a['dihedral']))
        d = getDiff(a, g, 'LJ-14', absolute)
        print("LJ-14:      % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['LJ-14'],
                                                       a['LJ-14']))
        d = getDiff(a, g, 'C-14', absolute)
        print("Coulomb-14: % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['C-14'],
                                                       a['C-14']))
        d = getDiff(a, g, 'LJ', absolute)
        print("LJ:         % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['LJ'],
                                                       a['LJ']))
        d = getDiff(a, g, 'C', absolute)
        print("Coulomb:    % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['C'],
                                                       a['C']))
        d = getDiff(a, g, 'potential', absolute)
        print("Potential:  % 15.4f % 15.6f % 15.6f" % (d,
                                                       g['potential'],
                                                       a['potential']))


def compareAverage(fA, fG, absolute=True):
    bond, angle, dih, lj_14, c_14, lj, c, pot = [0.0 for i in range(8)]
    nframes = len(fA)
    for a, g in zip(fA, fG):
        if g['step'] in IGNORE_FRAMES:
            print("Ignoring step %i" % g['step'])
            continue

        if abs(a['bond']) > 1e-16:
            bond += getDiff(a, g, 'bond', absolute) / nframes
        if abs(a['angle']) > 1e-16:
            angle += getDiff(a, g, 'angle', absolute) / nframes
        if abs(a['dihedral']) > 1e-16:
            dih += getDiff(a, g, 'dihedral', absolute) / nframes
        if abs(a['LJ-14']) > 1e-16:
            lj_14 += getDiff(a, g, 'LJ-14', absolute) / nframes
        if abs(a['C-14']) > 1e-16:
            c_14 += getDiff(a, g, 'C-14', absolute) / nframes
        if abs(a['LJ']) > 1e-16:
            lj += getDiff(a, g, 'LJ', absolute) / nframes
        if abs(a['C']) > 1e-16:
            c += getDiff(a, g, 'C', absolute) / nframes
        if abs(a['potential']) > 1e-16:
            pot += getDiff(a, g, 'potential', absolute) / nframes

    u = "(kJ)" if absolute else "(%)"
    print("Average difference %s" % u)
    print("Bond:       %10.4f" % bond)
    print("Angle:      %10.4f" % angle)
    print("Dih.:       %10.4f" % dih)
    print("LJ-14:      %10.4f" % lj_14)
    print("Coulomb-14: %10.4f" % c_14)
    print("LJ:         %10.4f" % lj)
    print("Coulomb:    %10.4f" % c)
    print("Potential:  %10.4f" % pot)


def compareMax(fA, fG, absolute=True):
    bond, angle, dih, lj_14, c_14, lj, c, pot = [[0.0, -1] for i in range(8)]
    for a, g in zip(fA, fG):
        if g['step'] in IGNORE_FRAMES:
            continue

        d = getDiff(a, g, 'bond', absolute)
        if abs(d) > abs(bond[0]):
            bond = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'angle', absolute)
        if abs(d) > abs(angle[0]):
            angle = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'dihedral', absolute)
        if abs(d) > abs(dih[0]):
            dih = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'LJ-14', absolute)
        if abs(d) > abs(lj_14[0]):
            lj_14 = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'C-14', absolute)
        if abs(d) > abs(c_14[0]):
            c_14 = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'LJ', absolute)
        if abs(d) > abs(lj[0]):
            lj = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'C', absolute)
        if abs(d) > abs(c[0]):
            c = [d, g['step'], g['filename'].split('.')[0]]

        d = getDiff(a, g, 'potential', absolute)
        if abs(d) > abs(pot[0]):
            pot = [d, g['step'], g['filename'].split('.')[0]]

    u = "(kJ)" if absolute else "(%) "
    print("Max difference %s        frame    file" % u)
    print("Bond:       %10.4f %10i   %s" % tuple(bond))
    print("Angle:      %10.4f %10i   %s" % tuple(angle))
    print("Dih.:       %10.4f %10i   %s" % tuple(dih))
    print("LJ-14:      %10.4f %10i   %s" % tuple(lj_14))
    print("Coulomb-14: %10.4f %10i   %s" % tuple(c_14))
    print("LJ:         %10.4f %10i   %s" % tuple(lj))
    print("Coulomb:    %10.4f %10i   %s" % tuple(c))
    print("Potential:  %10.4f %10i   %s" % tuple(pot))


def compareMin(fA, fG, absolute=True):
    bond, angle, dih, lj_14, c_14, lj, c, pot = [[999.9, -1] for i in range(8)]
    for a, g in zip(fA, fG):
        if g['step'] in IGNORE_FRAMES:
            continue

        d = getDiff(a, g, 'bond', absolute)
        if abs(d) < abs(bond[0]):
            bond = [d, g['step']]

        d = getDiff(a, g, 'angle', absolute)
        if abs(d) < abs(angle[0]):
            angle = [d, g['step']]

        d = getDiff(a, g, 'dihedral', absolute)
        if abs(d) < abs(dih[0]):
            dih = [0.0, g['step']]

        d = getDiff(a, g, 'LJ-14', absolute)
        if abs(d) < abs(lj_14[0]):
            lj_14 = [d, g['step']]

        d = getDiff(a, g, 'C-14', absolute)
        if abs(d) < abs(c_14[0]):
            c_14 = [d, g['step']]

        d = getDiff(a, g, 'LJ', absolute)
        if abs(d) < abs(lj[0]):
            lj = [d, g['step']]

        d = getDiff(a, g, 'C', absolute)
        if abs(d) < abs(c[0]):
            c = [d, g['step']]

        d = getDiff(a, g, 'potential', absolute)
        if abs(d) < abs(pot[0]):
            pot = [d, g['step']]

    u = "(kJ)" if absolute else "(%) "
    print("Min difference %s        frame" % u)
    print("Bond:       %10.4f %10i" % tuple(bond))
    print("Angle:      %10.4f %10i" % tuple(angle))
    print("Dih.:       %10.4f %10i" % tuple(dih))
    print("LJ-14:      %10.4f %10i" % tuple(lj_14))
    print("Coulomb-14: %10.4f %10i" % tuple(c_14))
    print("LJ:         %10.4f %10i" % tuple(lj))
    print("Coulomb:    %10.4f %10i" % tuple(c))
    print("Potential:  %10.4f %10i" % tuple(pot))


def getBondPot(mol):
    kb36 = 267776.0
    rb36 = 0.143
    kb37 = 259408.0
    rb37 = 0.152
    kb38 = 238488.0
    rb38 = 0.146
    bPot = 0.0
    bonds = {}
    bondIt = ob.OBMolBondIter(mol.OBMol)
    for bond in bondIt:
        a = bond.GetBeginAtomIdx()
        b = bond.GetEndAtomIdx()
        ats = [a, b]
        ats.sort()
        l = bond.GetLength()
        bonds[tuple(ats)] = l * 0.1
    bondI36 = [(2,3), (20,23), (13,15), (9,11), (5,7)]
    bondI37 = [(17,20), (13,17), (9,13), (5,9), (3,5)]
    bondI38 = [(17,19), (3,19)]
    for i in bondI36:
        bPot += 0.5*kb36*(bonds[i]-rb36)**2
    for i in bondI37:
        bPot += 0.5*kb37*(bonds[i]-rb37)**2
    for i in bondI38:
        bPot += 0.5*kb38*(bonds[i]-rb38)**2
    return bPot



if __name__ == '__main__':
    options, args = optP()
    if options.gromacsLog is None:
        print("Please specify Gromacs log file name (-g)!")
        sys.exit(2)
    if (options.amberLog is None and
        options.amberEnLog is None and
        options.amberRerunLog is None):
        print("Please specify Amber log file name (-a, -b or -e)!")
        sys.exit(2)

    absolute = not options.relativeDiff

    if options.gromacsMinLog:
        getGromacsNumbers = getGMNumbers
    if isinstance(options.gromacsLog, list):
        print("Found", len(options.gromacsLog), "Gromacs logs")
        framesGromacs = parseGromacsLogFiles(options.gromacsLog)
    else:
        framesGromacs = parseGromacsLog(options.gromacsLog)
    if options.amberEnLog is not None:
        framesAmber = parseAmberEnLog(options.amberEnLog)
    elif options.amberLog is not None:
        framesAmber = parseAmberLog(options.amberLog)
    elif options.amberRerunLog is not None:
        if isinstance(options.amberRerunLog, list):
            print("Found", len(options.amberRerunLog), "Amber rerun logs")
            framesAmber = parseAmberRerunLogFiles(options.amberRerunLog)
        else:
            framesAmber = parseAmberRerunLog(options.amberRerunLog)
    if options.discardFirst2:
        del framesAmber[:2]
        del framesGromacs[:2]
    elif options.discardFirst:
        del framesAmber[0]
        del framesGromacs[0]

    if options.cluster:
        framesAmber, framesGromacs = cluster(framesAmber, framesGromacs)

    if options.verbose:
        compare(framesAmber, framesGromacs, absolute)
    compareAverage(framesAmber, framesGromacs, absolute)
    compareMax(framesAmber, framesGromacs, absolute)
    compareMin(framesAmber, framesGromacs, absolute)

    if (options.mdtrj is not None and
        options.potOut is not None):
        fformat = os.path.splitext(options.mdtrj)[1][1:]
        mols = pybel.readfile(fformat, options.mdtrj)
        f = open(options.potOut, 'w')
        f.write("#Step      bPot      Amber    Gromacs\n")
        for n, mol in enumerate(mols):
            s = "%4i % 10.4f % 10.4f % 10.4f\n" % (n, getBondPot(mol),
                                                   framesAmber[n]['bond'],
                                                   framesGromacs[n]['bond'])
            f.write(s)
        f.close()
