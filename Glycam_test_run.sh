#!/bin/bash


#################################
# Preparing for Executing Amber #
#################################

# Make Amber available
export AMBERHOME="/home/reinis/Hack/amber12"

# Template for executing tleap
cat <<EOF > template.tleaprc
#source leaprc.ff99SBild
source leaprc.GLYCAM_06h
mol = loadpdb MOL.pdb
saveamberparm mol MOL.prmtop MOL.inpcrd
quit
EOF

# Sander simulation options
cat<< EOF > rerun.in
Monomer Sugar: Calculation energy frame
 &cntrl
  imin   = 1,
  maxcyc = 0,
  ntb    = 0,
  cut    = 999,
/
EOF

###############################
# Preparing files for Gromacs #
###############################

# TOP file
cat<<EOF > template.top
[ defaults ]
; nbfunc    comb-rule    gen-pairs    fudgeLJ fudgeQQ
1           2            yes          1       1

#include "glycam06h.itp"
#include "MOL.itp"

[ system ]
; name
Molecule: MOL

[ molecules ]
; name  number
sugar   1
EOF

# MDP file
cat<<EOF > rerun.mdp
integrator          =  md
dt                  =  0.001
nsteps              =  1

;Remove angular and linear momentum
comm_mode           = Angular

; Output files frequency
nstxout             =  1
nstlog              =  1
nstenergy           =  1

; Neighbor searching
nstlist             =  0
ns_type             =  simple
pbc                 =  no
rlist               =  100.0

;coulobm interactions
coulombtype         =  Cut-off
rcoulomb-switch     =  0
rcoulomb            =  100.0

; Van der Waals interactions
vdwtype             =  Cut-off
rvdw-switch         =  0
rvdw                =  100.0

; Temperature coupling
tcoupl              = no

; Bonds contrains
constraints         = none
EOF

################################################
# Loop over the structures: Energy Calculation #
################################################

runag() {
  NAME=${1}

  # AMBER
  cp template.tleaprc $NAME.tleaprc
  sed -i "s/MOL/${NAME}/g" $NAME.tleaprc
  ${AMBERHOME}/bin/tleap -f $NAME.tleaprc
  ${AMBERHOME}/bin/sander -O -i rerun.in -p $NAME.prmtop -c $NAME.inpcrd -o $NAME.out -r $NAME.restart -inf $NAME.mdinfo

  # GROMACS
  cp template.top $NAME.top
  sed -i "s/MOL/${NAME}/g" $NAME.top
  sed -i "s/sugar/${NAME%%_*}/g" $NAME.top
  grompp -f rerun.mdp -c $NAME.pdb -p $NAME.top -o $NAME.tpr -po ${NAME}_mdout.mdp
  mdrun -rerun $NAME.pdb -deffnm $NAME
}

export -f runag

parallel "runag {.}" ::: `ls *.pdb`

