#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.01
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Calculate total charge for each molecule in Gromacs topology and show how
the total charge is formed from each atom. It lets to check total charges
in charge groups, too.

Usage:
[python3] sum_ch.py topology_file
"""

import re
import sys

from decimal import Decimal


reXa = re.compile(r'^\s*[[]+\satoms\s[]]+')
reXi = re.compile(r'^\s*[[]+')
reXe = re.compile(r'^\s*$')
reXc = re.compile(r'^\s*[;#]+')

if len(sys.argv) == 1:
    print("Usage: [python3] sum_ch.py topology_file")
    print("\nError: No topology file given.")
    sys.exit(1)

if "-h" in sys.argv or "--help" in sys.argv:
    print(__doc__)
    sys.exit(1)

with open(sys.argv[1], 'r') as f:
    line = f.readline()
    while line != '':
        s = Decimal('0.000') # Total charge
        groups = {}          # Charge group total charges
        while reXa.match(line) is None:
            if line == '':
                break
            line = f.readline()
        if line == '':
            break

        line = f.readline()
        while reXi.match(line) is None:
            if line == '':
                break
            if (reXc.match(line) is not None
                or reXe.match(line) is not None):
                line = f.readline()
                continue
            line = line.split()
            d = Decimal(line[6]) # Atom charge
            n = line[5]          # Charge group
            if n in groups:
                groups[n] += d
                print(n, '+', d, '=', groups[n])
            else:
                print(n, '=', d)
                groups[n] = d
            s += d
            line = f.readline()

        print("Sum = ", s)
