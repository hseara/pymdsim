#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Version 0.02
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Conformation scanner.
"""

import re
import sys
import pybel
import os.path
import numpy as np
import openbabel as ob
import subprocess as sp

from optparse import OptionParser
from itertools import product
from multiprocessing import Pool



class GamConf(object):
    """Hold configuration settings for GAMESS runs.

    """
    def __init__(self):
        """Initialize the object.

        """
        # List of keyword groups
        self.groups = ['basis',
                       'contrl',
                       'pcm',
                       'scf',
                       'statpt',
                       'system',
                       'zmat']

        # How many variables in each group have to be set
        self.basis  = 0
        self.contrl = 0
        self.pcm    = 0
        self.scf    = 0
        self.statpt = 0
        self.system = 0
        self.zmat   = 0

        # Dictionaries of default values
        self.BASIS = {'GBASIS': [False, None],
                      'NGAUSS': [False, 0],
                      'NDFUNC': [False, 0],
                      'NPFUNC': [False, 0]}

        self.CONTRL = {'ICHARG': [False, 0],
                       'MULT':   [False, 1],
                       'RUNTYP': [False, 'ENERGY'],
                       'EXETYP': [False, 'RUN'],
                       'MAXIT':  [False, 30],
                       'NZVAR':  [False, 0]}

        self.PCM = {'SOLVNT': [False, 'WATER']}

        self.SCF = {'CONV':   [False, '1.0E-05'],
                    'DIRSCF': [False, '.F.']}

        self.STATPT = {'NSTEP': [False, 50],
                       'HESS':  [False, 'GUESS'],
                       'IHREP': [False, 0]}

        self.SYSTEM = {'TIMLIM': [False, 525600],
                       'MWORDS': [False, 1]}

        self.ZMAT = {'DLC':       [False, '.F.'],
                     'AUTO':      [False, '.F.'],
                     'IFZMAT(1)': [False, None],
                     'FVALUE':    [False, None]}


    def get(self, group, var):
        """Get a value of the setting in the group.

        """
        return getattr(self, group)[var][1]


    def set(self, group, var, value):
        """Set value for the setting in the group.

        Also increment the counter of changed settings and show that the
        particular value has changed.
        """
        getattr(self, group)[var] = [True, value]
        a = getattr(self, group.lower())
        a += 1
        setattr(self, group.lower(), a)


    def show(self):
        """Return string showing how the configuration looks.

        """
        s = ""
        for i in self.groups:
            if getattr(self, i) > 0:
                s += " $%s" % i.upper()
                a = 2 + len(i)
                for k, v in getattr(self, i.upper()).items():
                    val = str(v[1])
                    if a + 2 + len(k) + len(val) > 72:
                        a = 5 + len(k) + len(val)
                        s += "\n   "
                    else:
                        a += 2 + len(k) + len(val)
                    if v[0]:
                        s += " %s=%s" % (k, val)
                s += " $END\n"
        return s


    def make_gms_keys(self):
        """Return OBSetData object with GAMESS keys.

        """
        gms_keys = ob.OBSetData()
        gms_keys.SetAttribute("gamess")

        for g in self.groups:
            if getattr(self, g) > 0:
                gms_key_group = ob.OBSetData()
                gms_key_group.SetAttribute(g.upper())
                for k, v in getattr(self, g.upper()).items():
                    if v[0]:
                        gms_key = ob.OBPairData()
                        gms_key.SetAttribute(k)
                        gms_key.SetValue(str(v[1]))
                        gms_key_group.AddData(gms_key.Clone(None))
                gms_keys.AddData(gms_key_group.Clone(None))

        return gms_keys



def optP():
    """Create and initialize Option Parser.

    """
    usage = '[python] %prog [-CdMnoOqQrsvw] -t i1 i2 i3 i4 [...] input_file'
    version = '\n%prog Version 0.02\n\nRequires Python 2.6 ' \
              'or newer.'
    description = 'Conformation scanner. Read one coordinate file,' \
                  ' rotate around given torsion(s) and write out new' \
                  ' conformations to files.'
    optParser = OptionParser(usage=usage,
                             version=version,
                             description=description)
    optParser.set_defaults(verbose=False, step=10, nsteps=100, overw=False,
                           optimize=False, quanta=0, queue=None, run=False,
                           charge=0, mult=1, torsionValues=None, solw=False)
    optParser.add_option('-v', action='store_true',
                         dest='verbose',
                         help='Be verbose [default: %default]')
    optParser.add_option('-r', action='store_true',
                         dest='run',
                         help='Run QM calculations on localhost '
                         '[default: %default]')
    optParser.add_option('-d', type='int',
                         dest='step',
                         help='Step size for torsion [default: %default]')
    optParser.add_option('-n', type='int',
                         dest='nsteps',
                         help='Number of steps for optimization '
                         '[default: %default]')
    optParser.add_option('-t', action='callback', callback=getAIs,
                         dest='torsions',
                         help='Ids of atoms defining torsion(s) '
                         '[default: %default]')
    optParser.add_option('-o', action='store_true',
                         dest='overw',
                         help='Overwrite existing files [default: %default]')
    optParser.add_option('-O', action='store_true',
                         dest='optimize',
                         help='Optimize each conformation [default: %default]')
    optParser.add_option('-q', type='int',
                         dest='queue',
                         help='Submit jobs to a batch server '
                         '[default: %default]')
    optParser.add_option('-Q', type='int',
                         dest='quanta',
                         help='Level of QM optimizations [default: %default]')
    optParser.add_option('-C', type='int',
                         dest='charge',
                         help='Charge of the molecule [default: %default]')
    optParser.add_option('-M', type='int',
                         dest='mult',
                         help='Multiplicity of the molecule '
                         '[default: %default]')
    optParser.add_option('-s', action='callback', callback=getDihVals,
                         dest='torsionValues',
                         help='Set given torsion to this value '
                         '[default: %default]')
    optParser.add_option('-w', action='store_true',
                         dest='solw',
                         help='Use implicit solvent (water) '
                         '[default: %default]')

    options, args = optParser.parse_args()

    if options.torsions is None:
        optParser.print_usage()
        sys.exit(1)

    if len(options.torsions) % 4 != 0:
        print("Error: Number of atom indices has to be a multiple of 4!")
        sys.exit(2)

    if options.torsionValues is not None:
        # Check if number of torsions is the same as number of values
        g = len(options.torsionValues)
        e = int(len(options.torsions) / 4)
        if e != g:
            print("Error: Expected %i torsion values but got %i!" % (e, g))
            sys.exit(3)

    return options, args


def getAIs(option, opt_str, value, parser):
    """Extract dihedral indices from command line.

    """
    reX = re.compile(r'^[0-9]+$')
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if reX.match(arg) is None:
            break
        else:
            value.append(int(arg))
            del rargs[0]
    setattr(parser.values, option.dest, value)


def getDihVals(option, opt_str, value, parser):
    """Extract dihedral values from command line.

    """
    reX = re.compile(r'^[-0-9.]+$')
    value = []
    rargs = parser.rargs
    while rargs:
        arg = rargs[0]
        if reX.match(arg) is None:
            break
        else:
            value.append(int(arg))
            del rargs[0]
    setattr(parser.values, option.dest, value)


def setTorsion(mol, i1, i2, i3, i4, angle):
    """Set a torsion to a given value.

    """
    a1 = mol.GetAtom(i1)
    a2 = mol.GetAtom(i2)
    a3 = mol.GetAtom(i3)
    a4 = mol.GetAtom(i4)
    mol.SetTorsion(a1, a2, a3, a4, angle)


def makeConstr(torsions, angles):
    """Make forcefield constraints for torsions.

    """
    constr = ob.OBFFConstraints()
    for i, angle in enumerate(angles):
        i1, i2, i3, i4 = torsions[i:i+4]
        constr.AddTorsionConstraint(i1, i2, i3, i4, angle)

    return constr


def cOptMol(mol, constr, ff, nsteps):
    """Optimize molecule using MM forcefield and constraints.

    """
    obff = ob.OBForceField.FindForceField(ff)
    obff.Setup(mol, constr)
    obff.SteepestDescent(nsteps)
    obff.UpdateCoordinates(mol)

    return obff


def writeGamIn(mol, finp, torsions, angles, options):
    """Write GAMESS input file.

    """
    o = GamConf()
    o.set('CONTRL', 'ICHARG', options.charge)
    o.set('CONTRL', 'MULT',   options.mult)
    o.set('CONTRL', 'RUNTYP', 'OPTIMIZE')
    o.set('CONTRL', 'EXETYP', 'RUN')
    o.set('CONTRL', 'MAXIT',  200)
    o.set('CONTRL', 'NZVAR',  3*mol.NumAtoms()-6)

    o.set('SCF', 'CONV',   '1.0E-08')
    o.set('SCF', 'DIRSCF', '.T.')

    if options.solw:
        o.set('PCM', 'SOLVNT', 'WATER')

    if options.quanta == 1:
        o.set('SYSTEM', 'TIMLIM', 180)
        o.set('SYSTEM', 'MWORDS', 15)
        o.set('BASIS', 'GBASIS', 'AM1')
    elif options.quanta == 2:
        o.set('SYSTEM', 'TIMLIM', 1800)
        o.set('SYSTEM', 'MWORDS', 350)
        o.set('BASIS', 'GBASIS', 'N31')
        o.set('BASIS', 'NGAUSS', 6)
        o.set('BASIS', 'NDFUNC', 1)
        o.set('BASIS', 'NPFUNC', 1)
    elif options.quanta == 3:
        o.set('CONTRL', 'MPLEVL', 2)
        o.set('SYSTEM', 'TIMLIM', 10080)
        o.set('SYSTEM', 'MWORDS', 350)
        o.set('SYSTEM', 'MEMDDI', 3000)

    o.set('STATPT', 'NSTEP', 2000)
    o.set('STATPT', 'HESS',  'CALC')
    o.set('STATPT', 'IHREP', 10)

    o.set('ZMAT', 'DLC',       '.T.')
    o.set('ZMAT', 'AUTO',      '.T.')
    o.set('ZMAT', 'IFZMAT(1)',
          ','.join(' 3,{0[0]},{0[1]},{0[2]},{0[3]}'.format(torsions[n:n+4])
                       for n in range(0, len(torsions), 4)))
    o.set('ZMAT', 'FVALUE(1)',    ','.join(str(a) for a in angles))

    mol.CloneData(o.make_gms_keys())
    converter = ob.OBConversion()
    converter.SetOutFormat('gamin')
    converter.SetOptions('k', converter.GENOPTIONS)
    converter.WriteFile(mol, finp)
    mol.DeleteData("gamess")


def doScanTorsions(pybmol, t, step, options, writer='MM'):
    """Generate input files for torsion scans.

    Optionally optimize molecule using MM before writing structures.
    """
    ndihs = int(len(t) / 4)

    for angles in product(range(0, 359, step), repeat=ndihs):
        if any(abs(a - 180) < 1 for a in angles):
            continue
        sangles = '_'.join(str(a) for a in angles)

        # Set torsion values
        for n in range(ndihs):
            i1, i2, i3, i4 = t[n:n+4]
            setTorsion(pybmol.OBMol, i1, i2, i3, i4, np.radians(angles[n]))

        if options.verbose:
            print(angles)
            for n in range(ndihs):
                i1, i2, i3, i4 = t[n:n+4]
                print(pybmol.OBMol.GetTorsion(i1, i2, i3, i4))

        # Run MM optimization if requested
        if options.optimize:
            constraints = makeConstr(t, angles)
            obff = cOptMol(pybmol.OBMol, constraints, 'MMFF94', options.nsteps)
            pybmol.OBMol.SetTitle("%s E=%f %s" % (fname,
                                                  obff.Energy(),
                                                  obff.GetUnit()))

            if options.verbose:
                print(angles)
                for n in range(ndihs):
                    i1, i2, i3, i4 = t[n:n+4]
                    print(pybmol.OBMol.GetTorsion(i1, i2, i3, i4))

        # Write structure to file
        if writer == 'QM':
            finp = '%s_%si.inp' % (fname, sangles)
            angles = list(angles)
            for n, angle in enumerate(angles):
                if angle > 180:
                    angles[n] -= 360
            writeGamIn(pybmol.OBMol, finp, t, angles, options)
        else:
            pybmol.write(fformat, "%s_%sa.%s" % (fname, sangles, fformat),
                         overwrite=options.overw)


def runQ(args):
    """Run given GAMESS calculation serially.

    """
    angles = args[0]
    suffix = args[1]

    if any(abs(a - 180) < 1 for a in angles):
        return

    print('Started', angles)

    sangles = '_'.join(str(a) for a in angles)
    finp    = '%s_%s%s.inp' % (fname, sangles, suffix)
    pargs   = ['rungms', finp, '00', '1']

    with open('%s_%s%s.log' % (fname, sangles, suffix), 'w') as fout:
        retcode = sp.call(pargs, stdout=fout, stderr=fout)
        print(angles, retcode)

    return retcode


def repeat(x):
    """Generator to repeat input infinitely.

    """
    while True:
        yield x


def poolQ(step, suffix, processes=None, ndihs=1):
    """Run serial GAMESS calculations in a process pool.

    """
    pool = Pool(processes)
    result = pool.map(runQ, zip(product(range(0, 359, step), repeat=ndihs),
                                repeat(suffix)))
    return result


def runScanTorsions(pybmol, t, step, options, writer="MM", offset=0):
    """Run QM calculations for torsion scans.

    """
    N = options.queue
    # Run on localhost if there is no queue
    if N is None:
        ret = poolQ(step, 'i', ndihs=int(len(t)/4))
        if any(ret):
            print("Error: Some of the calculations failed!")
            print(ret)
            sys.exit(1)
        else:
            sys.exit(0)

    # Add jobs to queue
    for angles in product(range(0, 359, step), repeat=int(len(t)/4)):
        if any(abs(a - 180) < 1 for a in angles):
            continue
        sangles = '_'.join(str(a) for a in angles)
        finp = '%s_%si.inp' % (fname, sangles)
        pargs = ['rungms', finp, '00', '1']
        if N > 0:
            fsub = '%s_%si_qsub' % (fname, sangles)
            with open(fsub, 'w') as f:
                f.write('#!/bin/sh\n')
                f.write('#PBS -N %s_%si\n' % (fname, sangles))
                f.write('#PBS -l nodes=1:ppn=%i\n' % N)
                f.write('#PBS -l walltime=48:00:00\n')
                f.write('#PBS -m n\n')
                f.write('cd %s\n' % os.getcwd())
                f.write('%s %s %s %i > %s_%si.log\n' % (pargs[0], pargs[1],
                                                        pargs[2], N,
                                                        fname,    sangles))
            sp.Popen(['qsub', fsub])
        else:
            print("Error: Wrong queue type!")
            sys.exit(1)


def scanTorsions(pybmol, t, step, options):
    """Scan requested torsion profiles.

    """
    if options.quanta >= 1:
        writer = "QM"
    else:
        writer = "MM"

    # Scan all torsions
    doScanTorsions(pybmol, t, step, options, writer=writer)

    if options.run:
        # Run QM calculations
        runScanTorsions(pybmol, t, step, options, writer=writer)



if __name__ == '__main__':
    options, args = optP()

    filename = args[0]
    fformat  = os.path.splitext(filename)[1][1:]

    pybmol = next(pybel.readfile(fformat, filename))

    fname = os.path.basename(filename)
    fname = os.path.splitext(fname)[0]

    t = options.torsions

    if options.torsionValues is not None:
        # Only set tortion values, don't scan
        angles  = options.torsionValues
        sangles = '_'.join(str(a) for a in angles)
        finp    = '%s_%si.inp' % (fname.split('_')[0], sangles)

        # Normalize torsion values
        for i, angle in enumerate(angles):
            angle %= 360
            if abs(angle) > 180:
                if angle < 0:
                    angles[i] = angle + 360
                else:
                    angles[i] = angle - 360

        # Set torsion values
        for n, angle in enumerate(angles):
            i1, i2, i3, i4 = t[n:n+4]
            setTorsion(pybmol.OBMol, i1, i2, i3, i4, np.radians(angle))

        # Write structure file
        if options.quanta >= 1:
            writeGamIn(pybmol.OBMol,
                       finp,
                       t,
                       angles,
                       options)
        else:
            pybmol.write(fformat, "%s_%ia.%s" % (fname, angle, fformat),
                         overwrite=options.overw)
        sys.exit(0)

    # Scan torsions
    scanTorsions(pybmol, t, options.step, options)
