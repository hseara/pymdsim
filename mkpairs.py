#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.01
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Read Gromacs topology and write out pairs list with explicit parameters with
correct scaling.
"""

import re
import sys

from math import sqrt
from optparse import OptionParser


def getV(pair):
    """Combine sigmas of the pair using combination rule 2.

    """
    return (nbp[ats[pair[0]]][0] + nbp[ats[pair[1]]][0]) / 2


def getW(pair, scnb=1.0):
    """Combine epsilons of the pair using combination rule 2.

    Optionally scale the value.
    """
    return sqrt(nbp[ats[pair[0]]][1] * nbp[ats[pair[1]]][1]) / scnb


#Parse command line options.
usage="[python3] %prog -p topology_file -t atomtype_file"
description="Make pair list with explicit parameters."
version="\n%prog Version 0.01\n\nRequires Python 3.0 or newer."
optParser = OptionParser(usage=usage,
                         version=version,
                         description=description)

optParser.set_defaults(itpFN=None, nbpFN=None, noSC=False)
optParser.add_option('-p', type='str',
                     dest='itpFN',
                     help="Gromacs topology file [default: %default]")
optParser.add_option('-t', type='str',
                     dest='nbpFN',
                     help="Glycam parameter file [default: %default]")
optParser.add_option('-N', action='store_true',
                     dest='noSC',
                     help="Ignore scaling factors in parameter file and use"
                     " no scaling [default: %default]")

options, args = optParser.parse_args()

# Check if required parameters are given
if options.itpFN is None or options.nbpFN is None:
    print(optParser.usage)
    sys.exit(1)

# Regular expressions used
reXt = re.compile(r'^[^;]*\[\s+atomtypes\s+\]')
reXd = re.compile(r'^[^;]*\[\s+dihedraltypes\s+\]')
reXa = re.compile(r'^[^;]*\[\s+atoms\s+\]')
reXp = re.compile(r'^[^;]*\[\s+pairs\s+\]')
reXc = re.compile(r'^\s*[;]+')
reXe = re.compile(r'^\s*$')
reXs = re.compile(r'^[^;]*\[\s+[a-z]*\s+\]')

# Read Glycam forcfield file and extract non-bonded parameters
nbp = {}
sc14 = {}
with open(options.nbpFN, 'r') as f:
    # Find [ atomtypes ]
    line = f.readline()
    while line != '' and reXt.match(line) is None:
        line = f.readline()
    # Read atomtypes
    line = f.readline()
    while line != '' and reXs.match(line) is None:
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        atype, atnum, mass, charge, ptype, sigma, epsilon = line.split()[:7]
        nbp[atype] = [float(sigma), float(epsilon)]
        line = f.readline()

    # Find [ dihedraltypes ]
    line = f.readline()
    while line != '' and reXd.match(line) is None:
        line = f.readline()

    # Read scaling factors
    line = f.readline()
    while line != '' and reXs.match(line) is None:
        if (reXc.match(line) is not None
            or reXe.match(line) is not None):
            line = f.readline()
            continue
        try:
            a, b, c, d, fn, p, kd, pn, com, scee, scnb = line.split()[:11]
        except ValueError:
            line = f.readline()
            continue
        if scee[:5] != 'SCEE=' or scnb[:5] != 'SCNB=':
            line = f.readline()
            continue
        sc14[(a, d)] = (float(scee[5:]), float(scnb[5:]))
        line = f.readline()

# Read topology, get atom types, numbers and charges
ats = {}
pairs = []
with open(options.itpFN, 'r') as f:
    # Find [ atoms ]
    line = f.readline()
    while reXa.match(line) is None:
        line = f.readline()
    # Read atoms
    line = f.readline()
    while reXe.match(line) is None:
        if reXc.match(line) is not None:
            line = f.readline()
            continue
        anr, atype, resnr, resn, aname, chgr, ch = line.split()[:7]
        ats[int(anr)] = atype
        try:
            nbp[atype].append(float(ch))
        except KeyError:
            pass
        line = f.readline()
    # Find  [ pairs ]
    while reXp.match(line) is None:
        line = f.readline()
    # Read pairs
    line = f.readline()
    while reXe.match(line) is None:
        if reXc.match(line) is not None:
            line = f.readline()
            continue
        a1, a2 = line.split()[:2]
        pairs.append([int(a1), int(a2)])
        line = f.readline()

# Print pairs list with explicit parameters
s1 = "{0[0]:6d} {0[1]:6d} {1:6d} {2:9.4f} {3:9.4f} {4:9.4f} {5:14.5e} {6:14.5e}"
s2 = "{0[0]:6d} {0[1]:6d}      1"
for i in pairs:
    try:
        scee = 1.0
        scnb = 1.0
        if not options.noSC:
            for p in sc14.keys():
                if ({ats[i[0]], ats[i[1]]} == {p[0], p[1]}):
                    scee, scnb = sc14[p]
                    break
        print(s1.format(i,
                        2,
                        1/scee,
                        nbp[ats[i[0]]][2],
                        nbp[ats[i[1]]][2],
                        getV(i),
                        getW(i, scnb)))
    except KeyError:
        print(s2.format(i))
