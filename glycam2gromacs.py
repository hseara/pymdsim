#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Version 0.02
Author: Reinis Danne <rei4dan@gmail.com>
License GPLv3

Convert Amber forcefield parameters to Gromacs format.
"""

import os.path
import sys
import re
import decimal
D = decimal.Decimal

from datetime import date
from optparse import OptionParser
from collections import OrderedDict


# Parse command line options
usage="[python3] %prog [-frs] -i inp_file -o out_file"
description="Convert Amber forcefield parameters to Gromacs format."
version="\n%prog Version 0.02\n\nRequires Python 3.0 or newer."
optParser = OptionParser(usage=usage,
                         version=version,
                         description=description)
optParser.set_defaults(inpFN=None, outFN=None,
                       overwrite=False, rmIncomplete=False, suffix=False,
                       unsupported=False)
optParser.add_option('-f', action='store_true',
                     dest='overwrite',
                     help="Overwrite existing file [default: %default]")
optParser.add_option('-i', type='str',
                     dest='inpFN',
                     help="Input filename [default: %default]")
optParser.add_option('-o', type='str',
                     dest='outFN',
                     help="Output filename [default: %default]")
optParser.add_option('-r', action='store_true',
                     dest='rmIncomplete',
                     help="Remove bonded parameters with atoms with"
                     " incomplete non-bonded parameters [default: %default]")
optParser.add_option('-s', action='store_true',
                     dest='suffix',
                     help="Add suffix '_S' for atom types [default: %default]")
optParser.add_option('-p', action='store_true',
                     dest='prefix',
                     help="Add prefix 'G_' for atom types [default: %default]")
optParser.add_option('-U', action='store_true',
                     dest='unsupported',
                     help="Don't filter out unsupported atomtypes"
                     " [default: %default]")

options, args = optParser.parse_args()


# Check options
if options.inpFN is None:
    print("Error: Missing input file name!")
    print(usage)
    sys.exit(1)

if not os.path.isfile(options.inpFN):
    print("Error: Missing input file", options.inpFN)
    print(usage)
    sys.exit(2)

if options.outFN is None:
    print("Error: Missing output file name!")
    print(usage)
    sys.exit(3)

if os.path.isfile(options.outFN) and not options.overwrite:
    print("Error: Output file {} already present!".format(options.outFN))
    print("Use command line flag '-f' to force overwriting.")
    sys.exit(4)


# Atom symbols and masses
def getAtomNumber(name):
    if name[0] == 'H':
        return 1
    elif name[0] == 'O':
        return 8
    elif name == 'F':
        return 9
    elif name == 'I':
        return 53
    elif name[0] == 'S':
        return 16
    elif name[0] == 'P':
        return 15
    elif name[0] == 'C':
        if len(name) > 1 and name[1] not in 'adeflorsu':
            return 6
        elif len(name) == 1:
            return 6
    elif name[0] == 'N':
        if len(name) > 1 and name[1] not in 'abdio':
            return 7
        elif len(name) == 1:
            return 7
    else:
        print("Warning: unrecognized element", name)
        return 118


# Regular expression for empty line
reX = re.compile(r'^\s*$')

# Get a filename as parameter from commandline
# Read Amber parameter file
f = open(options.inpFN, 'r')

# Title
line = f.readline()
title = line[:-1]

# Atoms
atoms = OrderedDict()
line = f.readline()
while reX.match(line) is None:
    par = line.split()
    atoms[par[0]] = [getAtomNumber(par[0]), D(par[1])]
    line = f.readline()
line = f.readline()

# Hydrophilic atoms
line = f.readline()

# Bonds
# angstroms -> nm
# kcal/mol/(A**2) -> kJ/mol/(nm**2)
kcalA2kJnm = D('418.400')
A2nm = D('0.1')
def setBond(bonds, line):
    a = line[:2].strip()
    b = line[3:5].strip()
    par = line[5:25].split()
    s = "{}-{}".format(a, b)
    bonds[s] = [a,
                b,
                D(par[0]) * kcalA2kJnm * 2,
                D(par[1]) * A2nm]
    bonds[s].append(line[28:-1])
    return

bonds = OrderedDict()
line = f.readline()
while reX.match(line) is None:
    setBond(bonds, line)
    line = f.readline()

# Angles
# degrees
# kcal/mol/(rad**2) -> kJ/mol/(rad**2)
kcal2kJ = D('4.184')
def setAngle(angles, line):
    a = line[:2].strip()
    b = line[3:5].strip()
    c = line[6:9].strip()
    par = line[9:30].split()
    s = "{}-{}-{}".format(a, b, c)
    angles[s] = [a,
                 b,
                 c,
                 D(par[0]) * kcal2kJ * 2,
                 D(par[1])]
    angles[s].append(line[31:-1])
    return

angles = OrderedDict()
line = f.readline()
while reX.match(line) is None:
    setAngle(angles, line)
    line = f.readline()

# Dihedrals
# degrees
def setDihedral(dihedrals, line):
    a = line[:2].strip()
    b = line[3:5].strip()
    c = line[6:8].strip()
    d = line[9:11].strip()
    par = line[11:78].split()
    s = "{}-{}-{}-{}".format(a, b, c, d)
    dihedrals[s] = [a,
                    b,
                    c,
                    d,
                    D(par[0]),
                    D(par[1]) * kcal2kJ,
                    D(par[2]),
                    D(par[3])]
    dihedrals[s].append(line[60:-1])
    while line[48] == '-':
        line = f.readline()
        par = line[11:78].split()
        dihedrals[s].extend([D(par[0]),
                             D(par[1]) * kcal2kJ,
                             D(par[2]),
                             D(par[3])])
        dihedrals[s].append(line[78:-1])
    return

dihedrals = OrderedDict()
line = f.readline()
while reX.match(line) is None:
    setDihedral(dihedrals, line)
    line = f.readline()

# Impropers
def setImproper(impropers, line):
    a = line[:2].strip()
    b = line[3:5].strip()
    c = line[6:8].strip()
    d = line[9:11].strip()
    par = line[11:78].split()
    s = "{}-{}-{}-{}".format(a, b, c, d)
    impropers[s] = [a,
                    b,
                    c,
                    d,
                    D(par[0]) * kcal2kJ,
                    D(par[1]),
                    D(par[2])]
    impropers[s].append(line[60:-1])
    return

impropers = OrderedDict()
line = f.readline()
while reX.match(line) is None:
    setImproper(impropers, line)
    line = f.readline()

# H-bond potential parameters
line = f.readline()
line = f.readline()

# Equivalent atom symbols
line = f.readline()
while reX.match(line) is None:
    line = f.readline()

# 6-12 potential parameters
line = f.readline()
if line.split()[1] != 'RE':
    print("Error: expected 'RE' for nb parameters")
    sys.exit(1)

# Rvdw2sigma = 1.0/(2**(1.0/6.0))*2 = 1.7817974362806785
Rvdw2sigma = D('1.7817974362806785')
def setVdWParam(vdwParams, line):
    a = line[:5].strip()
    par = line[:40].split()
    vdwParams[a] = [D(par[1]) * A2nm * Rvdw2sigma,
                    D(par[2]) * kcal2kJ,
                    line[40:-1].lstrip()]

vdwParams = OrderedDict()
line = f.readline()
while reX.match(line) is None:
    setVdWParam(vdwParams, line)
    line = f.readline()

# Skip the rest
f.close()


def isIncomplete(atoms):
    """Check if any of the provided atomtypes are missing non-boned parameters.

    Ignores wildcard atomtype 'X'.
    """
    for a in atoms:
        if a not in vdwParams and a != 'X':
            return True

    return False


def isAmber(atom,
            aAmber=['H', 'H1', 'H2', 'C', 'N3', 'O', 'O2', 'P', 'S']):
    """Check if the atom type is already in Amber.

    """
    return atom in aAmber


def isUnsupported(atoms, reXn=re.compile(r'^[0-9]+')):
    """Check if any of the provided atomtypes are not supported by Gromacs.

    Gromacs doesn't allow atomtype to start with a number.
    """
    for a in atoms:
        if ((reXn.match(a) is not None
             or a in ['HW', 'NH', 'OW'])
            and not options.unsupported):
            # Filter out also water atomtypes and undefined type NH
            return True

    return False


def decorate(atom):
    """Add prefix or suffix to atomtypes if requested.

    Don't rename 'X' since it has special meaning in Gromacs.
    Also avoid renaming general Amber atomtypes.
    """
    if (options.prefix
        and atom in vdwParams
        and atom not in ['H', 'H1', 'H2', 'C', 'N3', 'O', 'O2', 'P', 'S']
        and atom != 'X'):
        return "G_" + atom
    elif (options.suffix
          and atom in vdwParams
          and atom not in ['H', 'H1', 'H2', 'C', 'N3', 'O', 'O2', 'P', 'S']
          and atom != 'X'):
        return atom + "_S"
    else:
        return atom


# Convert parameters to Gromacs format
# Nonbonded
bonded = '; ' + title + '\n'
bonded += '; Converted to Gromacs format on ' + date.today().isoformat() + '\n'
bonded += "\n\n[ atomtypes ]\n"
bonded += "; name  at.num    mass     charge ptype sigma    epsilon\n"
s = "  {0:>4}   {1:2d}{2:15.3f}   0.0000 A   {3:1.5e}   {4:1.5e}   ; {5}\n"
for k, v in vdwParams.items():
    if isAmber(k) or isUnsupported([k]):
        continue
    bonded += s.format(decorate(k),
                       atoms[k][0],
                       atoms[k][1],
                       float(v[0]),
                       float(v[1]),
                       v[2])

# Bonded
bonded += "\n\n[ bondtypes ]\n"
bonded += "; i    j  func       b0          kb\n"
s = "  {0:>4} {1:>4}    1  {2:10.5f} {3:10.1f} ; {4}\n"
for k, v in bonds.items():
    if isUnsupported(v[:2]):
        continue
    if options.rmIncomplete and isIncomplete(v[:2]):
        continue
    bonded += s.format(decorate(v[0]),
                       decorate(v[1]),
                       v[3],
                       v[2],
                       v[4])

bonded += "\n\n[ angletypes ]\n"
bonded += ";  i    j    k  func       th0       cth\n"
s = "{0:>4}  {1:>4}  {2:>4}  1  {3:8.3f} {4:8.3f} ; {5}\n"
for k, v in angles.items():
    if isUnsupported(v[:3]):
        continue
    if options.rmIncomplete and isIncomplete(v[:3]):
        continue
    bonded += s.format(decorate(v[0]),
                       decorate(v[1]),
                       decorate(v[2]),
                       v[4],
                       v[3],
                       v[5])

bonded += "\n\n[ dihedraltypes ]\n"
bonded += ";i  j   k  l   func  phase  kd      pn\n"
s = "{0:>4}  {1:>4}  {2:>4}  {3:>4}   9  {4:5.1f} {5:10.5f} {6} ; {7}\n"
for k, v in dihedrals.items():
    if isUnsupported(v[:4]):
        continue
    if options.rmIncomplete and isIncomplete(v[:4]):
        continue
    bonded += s.format(decorate(v[0]),
                       decorate(v[1]),
                       decorate(v[2]),
                       decorate(v[3]),
                       v[6],
                       v[5] / v[4],
                       abs(v[7]),
                       v[8])
    n = 1
    v7 = float(v[7])
    while v7 < 0.0:
        v7 = float(v[5*n+3+4])
        bonded += s.format(decorate(v[0]),
                           decorate(v[1]),
                           decorate(v[2]),
                           decorate(v[3]),
                           v[5*n+3+3],
                           v[5*n+3+2] / v[5*n+3+1],
                           abs(v[5*n+3+4]),
                           v[5*n+3+5])
        n += 1

bonded += "\n\n[ dihedraltypes ]\n"
bonded += ";i  j   k  l   func\n"
s = "{0:>4}  {1:>4}  {2:>4}  {3:>4}   4  {4:5.1f} {5:10.5f} {6} ; {7}\n"
for k, v in impropers.items():
    if isUnsupported(v[:4]):
        continue
    if options.rmIncomplete and isIncomplete(v[:4]):
        continue
    bonded += s.format(decorate(v[0]),
                       decorate(v[1]),
                       decorate(v[2]),
                       decorate(v[3]),
                       v[5],
                       v[4],
                       v[6],
                       v[7])


# Write Gromacs parameter file
with open(options.outFN, 'w') as f:
    f.write(bonded)
